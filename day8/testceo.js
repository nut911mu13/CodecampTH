
const fs = require('fs');
const { Employee } = require('./testemployee.js');

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
    }
    getSalary() { // simulate public method
        return super.getSalary() * 2;
    };
    hello() { // simulate public method
        console.log("Hi, nice to meet you. " + this.firstname + "!");
    }
}
//let ceo = new CEO('Somchai', 'Sudlor', 30000);
//console.log(ceo.getSalary());

exports.CEO = CEO;