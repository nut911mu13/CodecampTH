const fs = require('fs');
class Employee {
    constructor(firstname, lastname, salary) {
        this._salary = salary; // simulate private variable
        this.firstname = firstname; // public property
        this.lastname = lastname; // public property

    }
    setSalary(newSalary) { // simulate public method
        if(newSalary > this._salary) {
            return newSalary
        } else {
            this._salary
            return false
        }
        
    }
    getSalary() {  // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
    gossip(employee, txt) {
        console.log("Hey " + employee.firstname + ", " + txt);
    }
}
exports.Employee = Employee;