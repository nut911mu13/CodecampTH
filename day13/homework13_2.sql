• จากการบ้านที่ 1 ข้อ 2 นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร จำนวนกี่คอร์ส (ให้ใช้แค่statement เดียวเท่านั้น)


select students.name,enrolls.student_id,sum(courses.price),count(courses.price) from courses
inner join enrolls on courses.id = enrolls.course_id
left join students on students.id = enrolls.student_id
group by students.id;

mysql> select enrolls.student_id,students.name,sum(courses.price),count(courses.price) from courses
    ->     inner join enrolls on courses.id = enrolls.course_id
    ->     left join students on students.id = enrolls.student_id
    ->     group by students.id
    -> ;
+------------+------------------+--------------------+----------------------+
| student_id | name             | sum(courses.price) | count(courses.price) |
+------------+------------------+--------------------+----------------------+
|        101 | Bojana Jankova   |                180 |                    2 |
|        102 | Michelle Salgado |                 90 |                    1 |
|        103 | Samita Thapa     |                 90 |                    1 |
|        104 | Jieun Han        |                180 |                    2 |
|        105 | Aranzazu Torres  |                 90 |                    1 |
|        106 | Jon Kaasa        |                 20 |                    1 |
|        107 | Karina Flores    |                 90 |                    1 |
|        108 | Steve Heim       |                 90 |                    1 |
|        109 | Pamela Chubb     |                 90 |                    1 |
|        110 | Daniel Lichty    |                110 |                    2 |
+------------+------------------+--------------------+----------------------+
10 rows in set (0.00 sec)




• นักเรียนแต่ละคน ซื้อคอร์สไหน ราคาแพงสุด


select enrolls.student_id,name, price from courses
inner join enrolls on courses.id = enrolls.course_id
where price = (select max(price) from courses);

mysql> select enrolls.student_id,name, price from courses
    -> inner join enrolls on courses.id = enrolls.course_id
    -> where price = (select max(price) from courses);
+------------+-------------------------+-------+
| student_id | name                    | price |
+------------+-------------------------+-------+
|        102 | Cooking                 |    90 |
|        110 | Cooking                 |    90 |
|        108 | Acting                  |    90 |
|        104 | Conservation            |    90 |
|        101 | Design and Architecture |    90 |
|        103 | Film Scoring            |    90 |
|        104 | Film Scoring            |    90 |
|        107 | Film Scoring            |    90 |
|        101 | Screenwriting           |    90 |
|        105 | Screenwriting           |    90 |
|        109 | Photography             |    90 |
+------------+-------------------------+-------+
11 rows in set (0.01 sec)

select s.name, c.name, max(c.price)
from enrolls e
left join students s on e.student_id = s.id
left join courses c on e.course_id = c.id
where c.price = (
select max(c1.price)
from enrolls e1
left join students s1 on e1.student_id = s1.id
left join courses c1 on e1.course_id = c1.id
where s1.id = s.id
)
group by s.name, c.name;
mysql> select s.name, c.name, max(c.price)
    -> from enrolls e
    -> left join students s on e.student_id = s.id
    -> left join courses c on e.course_id = c.id
    -> where c.price = (
    -> select max(c1.price)
    -> from enrolls e1
    -> left join students s1 on e1.student_id = s1.id
    -> left join courses c1 on e1.course_id = c1.id
    -> where s1.id = s.id
    -> )
    -> group by s.name, c.name;
+------------------+-------------------------+--------------+
| name             | name                    | max(c.price) |
+------------------+-------------------------+--------------+
| Aranzazu Torres  | Screenwriting           |           90 |
| Bojana Jankova   | Design and Architecture |           90 |
| Bojana Jankova   | Screenwriting           |           90 |
| Daniel Lichty    | Cooking                 |           90 |
| Jieun Han        | Conservation            |           90 |
| Jieun Han        | Film Scoring            |           90 |
| Jon Kaasa        | JavaScript for Beginner |           20 |
| Karina Flores    | Film Scoring            |           90 |
| Michelle Salgado | Cooking                 |           90 |
| Pamela Chubb     | Photography             |           90 |
| Samita Thapa     | Film Scoring            |           90 |
| Steve Heim       | Acting                  |           90 |
+------------------+-------------------------+--------------+
12 rows in set (0.02 sec)