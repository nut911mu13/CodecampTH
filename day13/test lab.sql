นักเรียนแต่ละคน ซื้อคอร์สไหน ราคาแพงสุด
select name, price
from courses
where price = (select max(price) from courses);

mysql> select name, price
    -> from courses
    -> where price = (select max(price) from courses);
+-------------------------------------+-------+
| name                                | price |
+-------------------------------------+-------+
| Cooking                             |    90 |
| Acting                              |    90 |
| Chess                               |    90 |
| Writing                             |    90 |
| Conservation                        |    90 |
| Tennis                              |    90 |
| The Art of Performance              |    90 |
| Writing #2                          |    90 |
| Building a Fashion Brand            |    90 |
| Design and Architecture             |    90 |
| Singing                             |    90 |
| Jazz                                |    90 |
| Country Music                       |    90 |
| Fashion Design                      |    90 |
| Film Scoring                        |    90 |
| Comedy                              |    90 |
| Writing for Television              |    90 |
| Filmmaking                          |    90 |
| Dramatic Writing                    |    90 |
| Screenwriting                       |    90 |
| Electronic Music Production         |    90 |
| Cooking #2                          |    90 |
| Shooting, Ball Handler, and Scoring |    90 |
| Photography                         |    90 |
+-------------------------------------+-------+
24 rows in set (0.01 sec)



• นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร
select enrolls.student_id,enrolls.course_id,courses.name,
courses.price from courses 
inner join enrolls on courses.id = enrolls.course_id
order by student_id;
mysql> select enrolls.student_id,enrolls.course_id,courses.name,
    -> courses.price from courses 
    -> inner join enrolls on courses.id = enrolls.course_id
    -> order by student_id;
+------------+-----------+-------------------------+-------+
| student_id | course_id | name                    | price |
+------------+-----------+-------------------------+-------+
|        101 |        10 | Design and Architecture |    90 |
|        102 |         1 | Cooking                 |    90 |
|        103 |        15 | Film Scoring            |    90 |
|        104 |         5 | Conservation            |    90 |
|        105 |        20 | Screenwriting           |    90 |
|        106 |        26 | JavaScript for Beginner |    20 |
|        107 |        15 | Film Scoring            |    90 |
|        108 |         2 | Acting                  |    90 |
|        109 |        24 | Photography             |    90 |
|        110 |        26 | JavaScript for Beginner |    20 |
+------------+-----------+-------------------------+-------+
10 rows in set (0.00 sec)

mysql> select courses.name,enrolls.course_id,enrolls.student_id,
    -> courses.price from courses 
    -> inner join enrolls on courses.id = enrolls.course_id
    -> order by courses.id;
+-------------------------+-----------+------------+-------+
| name                    | course_id | student_id | price |
+-------------------------+-----------+------------+-------+
| Cooking                 |         1 |        102 |    90 |
| Acting                  |         2 |        108 |    90 |
| Conservation            |         5 |        104 |    90 |
| Design and Architecture |        10 |        101 |    90 |
| Film Scoring            |        15 |        103 |    90 |
| Film Scoring            |        15 |        107 |    90 |
| Screenwriting           |        20 |        105 |    90 |
| Photography             |        24 |        109 |    90 |
| JavaScript for Beginner |        26 |        110 |    20 |
| JavaScript for Beginner |        26 |        106 |    20 |
+-------------------------+-----------+------------+-------+
10 rows in set (0.01 sec)

• จากคอร์สที่มีคนเรียนทั้งหมด ได้เงินเท่าไร

select sum(price) from courses
inner join enrolls on enrolls.course_id = courses.id;

mysql> select sum(price) from courses
    -> inner join enrolls on enrolls.course_id = courses.id;
+------------+
| sum(price) |
+------------+
|        760 |
+------------+
1 row in set (0.00 sec)

select sum(price) from courses 
inner join enrolls on courses.id = enrolls.course_id
order by courses.id;



• จากการบ้านที่ 1 ข้อ 2 นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร จำนวนกี่คอร์ส (ให้ใช้แค่statement เดียวเท่านั้น)

select enrolls.student_id,courses.price from courses inner join enrolls on courses.id = enrolls.course_id ;

mysql> select enrolls.student_id,courses.price from courses inner join enrolls on courses.id = enrolls.course_id ;
+------------+-------+
| student_id | price |
+------------+-------+
|        102 |    90 |
|        108 |    90 |
|        104 |    90 |
|        101 |    90 |
|        103 |    90 |
|        107 |    90 |
|        105 |    90 |
|        109 |    90 |
|        106 |    20 |
|        110 |    20 |
+------------+-------+
10 rows in set (0.00 sec)


select enrolls.student_id,students.name,sum(courses.price),count(courses.price) from courses
    inner join enrolls on courses.id = enrolls.course_id
    left join students on students.id = enrolls.student_id
    group by students.id



    