• เติมตารางนี้ให้เต็ม สำหรับ MySQL และ หาว่า isolation level ไหน คือค่า default 

-+------------------+--------------+----------------------+--------------+
| Level            | Dirty reads  | Non-repeatable reads |Phantoms reads|
+------------------+--------------+----------------------+--------------+
| READ UNCOMMITTED |       1      |    1                 |     1        |
| READ COMMITTED   |       0      |    1                 |     1        |
| REPEATABLE READ  |       0      |    0                 |     1        |
| SERIALIZABLE     |       0      |    0                 |     0        |
+------------------+--------------+----------------------+--------------+


mysql> SELECT @@GLOBAL.tx_isolation, @@tx_isolation;
+-----------------------+-----------------+
| @@GLOBAL.tx_isolation | @@tx_isolation  |
+-----------------------+-----------------+
| REPEATABLE-READ       | REPEATABLE-READ |
+-----------------------+-----------------+
1 row in set (0.01 sec)