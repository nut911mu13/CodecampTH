• จากตัวอย่าง JOIN ของเมื่อวาน (การบ้านเมื่อวาน)


• จากคอร์สที่มีคนเรียนทั้งหมด ได้เงินเท่าไร

select sum(price) from courses
inner join enrolls on courses.id = enrolls.course_id
order by courses.id;


mysql> select sum(price) from courses 
    -> inner join enrolls on courses.id = enrolls.course_id
    -> order by courses.id;
+------------+
| sum(price) |
+------------+
|        760 |
+------------+
1 row in set (0.01 sec)


mysql> select sum(price) from courses
    -> inner join enrolls on courses.id = enrolls.course_id
    -> order by courses.id;
+------------+
| sum(price) |
+------------+
|       1030 |
+------------+
1 row in set (0.00 sec)


• นักเรียนแต่ละคน ซื้อคอร์สไปคนละเท่าไร


select enrolls.student_id,enrolls.course_id,courses.name,
courses.price from courses 
inner join enrolls on courses.id = enrolls.course_id
group by student_id;
mysql> select enrolls.student_id,enrolls.course_id,courses.name,
    -> courses.price from courses 
    -> inner join enrolls on courses.id = enrolls.course_id
    -> group by student_id;
+------------+-----------+-------------------------+-------+
| student_id | course_id | name                    | price |
+------------+-----------+-------------------------+-------+
|        101 |        10 | Design and Architecture |    90 |
|        102 |         1 | Cooking                 |    90 |
|        103 |        15 | Film Scoring            |    90 |
|        104 |         5 | Conservation            |    90 |
|        105 |        20 | Screenwriting           |    90 |
|        106 |        26 | JavaScript for Beginner |    20 |
|        107 |        15 | Film Scoring            |    90 |
|        108 |         2 | Acting                  |    90 |
|        109 |        24 | Photography             |    90 |
|        110 |        26 | JavaScript for Beginner |    20 |
+------------+-----------+-------------------------+-------+
10 rows in set (0.02 sec)






select students.name,enrolls.student_id,sum(courses.price) from courses
inner join enrolls on courses.id = enrolls.course_id
left join students on students.id = enrolls.student_id
group by students.id
order by students.id;
mysql> select students.name,enrolls.student_id,sum(courses.price) from courses
    -> inner join enrolls on courses.id = enrolls.course_id
    -> left join students on students.id = enrolls.student_id
    -> group by students.id
    -> order by students.id;
+------------------+------------+--------------------+
| name             | student_id | sum(courses.price) |
+------------------+------------+--------------------+
| Bojana Jankova   |        101 |                180 |
| Michelle Salgado |        102 |                 90 |
| Samita Thapa     |        103 |                 90 |
| Jieun Han        |        104 |                180 |
| Aranzazu Torres  |        105 |                 90 |
| Jon Kaasa        |        106 |                 20 |
| Karina Flores    |        107 |                 90 |
| Steve Heim       |        108 |                 90 |
| Pamela Chubb     |        109 |                 90 |
| Daniel Lichty    |        110 |                110 |
+------------------+------------+--------------------+
10 rows in set (0.00 sec)

