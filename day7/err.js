let winston = require('winston');
const winLog = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        new winston.transports.File({ filename: 'combined.log' })
    ]
});
winLog.add(new winston.transports.Console({
    format: winston.format.simple()
}));
winLog.info('This is info log');
winLog.error('This is error log');