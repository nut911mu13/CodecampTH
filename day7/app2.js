const Koa = require('koa');
const app = new Koa();

const logger = require('koa-logger');



app.use(logger());
app.use (async (ctx, next) =>{
    try {
        await next();
        someUnknownFunction();
    } catch (err) {
        ctx.status = 400
        ctx.body = `Uh-oh: ${err.message}`
        console.log('Error handler:', err.message)
    }   
}) 
app.listen(3000);