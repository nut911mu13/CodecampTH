// let fs = require('fs')
let readData = require('./readData')
let addYearSalary = require('./addYearSalary')
let addNextSalary = require('./addNextSalary')

start()
async function start() {
    let readFile = await readData.readData('homework1.json')
    console.log(readFile);
    let employees = JSON.parse(readFile);
    addAdditionalFields(employees);
}

async function addAdditionalFields(employees) {
    let newEmployees = employees.map((row) => {
        return addYearSalary.addYearSalary(row);
    })
        .map((row) => {
        return addNextSalary.addNextSalary(row);
    })
    console.log(newEmployees);
} 
