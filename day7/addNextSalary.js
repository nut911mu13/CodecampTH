function addNextSalary(row) {
    let NextSalary = [row.salary, row.salary * 110 / 100, (row.salary * 110 / 100) * 110 / 100]
    return { ...row, NextSalary };
}
exports.addNextSalary = addNextSalary;