// let fs = require('fs')
let readData = require('./readData')
let addYearSalary = require('./addYearSalary')
let addNextSalary = require('./addNextSalary')

async function start() {
    let readFile = await readData.readData('homework1.json')
    // console.log(readFile);
    let employees = JSON.parse(readFile);
    let newEmployees = await addAdditionalFields(employees);
    newEmployees[0].salary = 0
    console.log('NEW', employees[0].salary)
    console.log('NEW', newEmployees[0].salary)

}

async function addAdditionalFields(employees) {
    return employees.map(addYearSalary.addYearSalary).map(addNextSalary.addNextSalary)
} 

start()
