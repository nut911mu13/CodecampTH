let fs = require('fs')
let employees =
    [
        { "id": "1001", 'firstname': 'Luke', 'lastname': 'Skywalker' },
        { "id": "1002", 'firstname': 'Tony', 'lastname': 'Stark' },
        { "id": "1003", 'firstname': 'Somchai', 'lastname': 'Jaidee' },
        { "id": "1004", 'firstname': 'Monkey D', 'lastname': 'Luffee' },
    ];
let companies = [
    { "id": "1001", "company": "Walt Disney" },
    { "id": "1002", "company": "Marvel" },
    { "id": "1003", "company": "Love2work" },
    { "id": "1004", "company": "One Piece" },
];
let salaries = [
    { "id": "1001", "salary": "40000" },
    { "id": "1002", "salary": "1000000" },
    { "id": "1003", "salary": "20000" },
    { "id": "1004", "salary": "9000000" },
];
let like = [
    { "id": "1001", "like": "apple" },
    { "id": "1002", "like": "banana" },
    { "id": "1003", "like": "orange" },
    { "id": "1004", "like": "papaya" },
];
let dislike = [
    { "id": "1001", "dislike": "banana" },
    { "id": "1002", "dislike": "orange" },
    { "id": "1003", "dislike": "papaya" },
    { "id": "1004", "dislike": "apple" },
];

let employeesDatabase = [];
//let myEmployees = [employees, companies, salaries, like, dislike];

for (let i = 0; i < employees.length; i++) {
    let obj = {};
    for (let key in employees[i]) {       
        obj[key] = employees[i][key];
    } 
    for (let key in companies[i]) {
        obj[key] = companies[i][key];   
    }    
   for (let key in salaries[i]) {
        obj[key] = salaries[i][key];
       
    }         
    for (let key in like[i]) {
        obj[key] = like[i][key]; 
    }            
    for (let key in dislike[i]) {
        obj[key] = dislike[i][key];
    }
    employeesDatabase.push(obj);
}

let json = JSON.stringify(employeesDatabase);

// console.log(em.log(ployeesDatabase);
console.log(json);

fs.writeFile('homework3-3.json',json,'utf8', function (err) {
});

