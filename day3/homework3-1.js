let fs = require('fs')
/*let p1 = new Promise(function (resolve, reject) {
    resolve('one');
});
let p2 = new Promise(function (resolve, reject) {
    resolve('two');
});
let p3 = new Promise(function (resolve, reject) {
    resolve('three');
});
let p4 = new Promise(function (resolve, reject) {
    resolve('three');
});*/

let readhead = new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', function (err, datahead) {
            if (err)
                reject(err);
            else
                resolve(datahead);
    });
});

let readbody = new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', function (err, databody) {
        if (err)
            reject(err);
        else
            resolve(databody);
    });
});

let readleg = new Promise(function (resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function (err, dataleg) {
        if (err)
            reject(err);
        else
            resolve(dataleg);
    });
});

let readfeet = new Promise(function (resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function (err, datafeet) {
        if (err)
            reject(err);
        else
            resolve(datafeet);
    });
});

/*let robot = function(data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt',data,'utf8', function (err) {
            if (err)
                reject(err);
            else
                resolve("finish");
        });
    });
}*/

Promise.all ([readhead,readbody,readleg,readfeet])
    .then(function (result) {
        //return robot (result)
    let str = result.join('\n')
        console.log(str); // result = ['head','body','leg','feet']
        fs.writeFile('robot.txt',str, 'utf8', function (err) {
    });
})
    .catch(function (error) {
        console.error("There's an error", error);
    
});
