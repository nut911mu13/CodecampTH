let fs = require('fs')

function readhead () {
    return new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', function (err, datahead) {
        if (err)
            reject(err);
        else
            resolve(datahead);
        });
    });
}

function readbody () {
    return new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', function (err, databody) {
        if (err)
            reject(err);
        else
            resolve(databody);
        });
   });
}

function readleg () {
    return new Promise(function (resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function (err, dataleg) {
        if (err)
            reject(err);
        else
            resolve(dataleg);
       });
   });
}

function readfeet () {
    return new Promise(function (resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function (err, datafeet) {
        if (err)
            reject(err);
        else
            resolve(datafeet);
       });
    });
}

async function copyFile() {
    try {
        let head = await readhead(); 
        let body = await readbody();   
        let leg = await readleg(); 
        let feet = await readfeet();
        let allofbody = head + '\n' + body + '\n' + leg + '\n' + feet +'\n'
        fs.writeFile('robot.txt', allofbody, 'utf8', function (err) {
            console.log(allofbody);   
        });
    } catch (error) {
        console.error(error);
    }
}
copyFile();
 

       
     
     
    
     
