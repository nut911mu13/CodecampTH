const Koa = require('koa')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const logger = require('koa-logger')

const router = new Router()

.post('/auth/signup', (ctx) => {
        ctx.body = 'signup'
    })
    .post('/auth/signin', (ctx) => {
        ctx.body = 'signin'
    })
    .get('/auth/signout', (ctx) => {
        ctx.body = 'signout'
    })
    .post('/auth/verify', (ctx) => {
        ctx.body = 'verify'
    })

// ## Upload
.post('/upload', (ctx) => {
    ctx.body = 'upload'
})

// ## User
.patch('/user/:id', (ctx) => {
        ctx.body = 'id'
    })
    .put('/user/:id/follow', (ctx) => {
        ctx.body = 'follow'
    })
    .delete('/user/:id/follow', (ctx) => {
        ctx.body = 'follow'
    })
    .get('/user/:id/follow', (ctx) => {
        ctx.body = '/user/:id/follow'
    })
    .get('/user/:id/followed', (ctx) => {
        ctx.body = '/user/:id/followed'
    })

// ## Tweet
.get('/tweet', (ctx) => {
        ctx.body = '/tweet'
    })
    .post('/tweet', (ctx) => {
        ctx.body = '/tweet'
    })
    .put('/tweet/:id/like', (ctx) => {
        ctx.body = '/tweet/:id/like'
    })
    .delete('/tweet/:id/like', (ctx) => {
        ctx.body = '/tweet/:id/like'
    })
    .post('/tweet/:id/retweet', (ctx) => {
        ctx.body = '/tweet/:id/retweet'
    })
    .put('/tweet/:id/vote/:voteId', (ctx) => {
        ctx.body = '/tweet/:id/vote/:voteId'
    })
    .post('/tweet/:id/reply', (ctx) => {
        ctx.body = '/tweet/:id/reply'
    })

// ## Notification
.get('/notification', (ctx) => {
    ctx.body = '/notification'
})

// ## Direct Message
.get('/message', (ctx) => {
        ctx.body = '/message'
    })
    .get('/message/:userId', (ctx) => {
        ctx.body = '/message/:userId'
    })
    .post('/message/:userId', (ctx) => {
        ctx.body = '/message/:userId'
    })

function signup(ctx) {
    ctx.body = 'signup'
}

function signin(ctx) {
    ctx.body = 'signup'
}

const app = new Koa()
app.use(logger())
app.use(bodyParser())
app.use(router.routes())
app.listen(3000)