"use strict";

const Koa = require('koa')
const session = require('koa-session')
const mysql = require('mysql2/promise')
const dbConfig = require('./config/db')


//const pool = mysql2.createPool(dbConfig)


const pool = mysql.createPool({
    host: 'localhost',
    port: '8889',
    user: 'root',
    password: 'root',
    database: 'sessions'
});

const sessionStore = {}
//let checkDB = 0;

const sessionConfig = {
    key: 'sess',
    maxAge: 3600 * 1000,
    httpOnly: true,
    store: {
        async get(key, maxAge, { rolling }){
        //     try { 
               const db = await pool.getConnection()
               const [row] = await db.execute('SELECT id,data from sessions where id = ?',[key]);
               console.log(key)
               console.log(row);
               db.release()
                if (row[0] !== undefined) {
                    return sessionStore[key] = JSON.parse(row[0].data);
                } else {
                    return sessionStore[key];
                }
                    
               //}
               //return sessionStore[key];
            //console.log(row)
           // console.log(result[0])
           
            // if (row[0] !== undefined) {
            //     let jsonObj = row[0].data; //return JSON.parse(result[0].data); 
            //     //console.log(jsonObj)
            //     //return sessionStore[key]
            //     return jsonObj;
            // } else {
            //     return sessionStore[key]
            // }

            // JSON.parse(result[0].data);    
        // // let String = {}
        // String.views = 1
        // console.log(String)
            // { "views": 1, "_expire": 1516361870727, "_maxAge": 3600000 }
        //let jsonObj = JSON.parse(result);        
        //     }catch (err) {
        //      return;
        //     }
             //return JSON.parse(result[0].data);        
        //     }              
        // //console.log(result[0].data);
        //return jsonObj;
            //return sessionStore[key]
        },

        async set(key, sess, maxAge, { rolling }) {
            //sessionStore[key] = sess
            const db = await pool.getConnection()
            //console.log(db);
            let sessStringData = JSON.stringify(sess);
            await db.execute('INSERT INTO sessions (id,data) values (?,?) on DUPLICATE KEY UPDATE data = ?',
            [key, sessStringData, sessStringData])
            db.release()
            sessionStore[key] = sess
             //console.log(sessStringData);
            //sessionStore[key] = sess
        },
           
        
        destroy(key) {
           delete sessionStore[key]
        }
        
    }
}

const app = new Koa()

app.keys = ['supersecret']

app
    .use(session(sessionConfig, app))
    .use(handler)
    .listen(3000)

async function handler(ctx,next) {
    if (ctx.patch === '/favicon.ico')
    return;
    let n = ctx.session.views || 0
    ctx.session.views = ++n

    try {
        ctx.body = sessionStore
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
    } finally {

    }
    //ctx.body = `${n} views`
}





