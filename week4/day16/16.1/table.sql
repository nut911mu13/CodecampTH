create table todos (
  id int auto_increment,
  todo_message varchar(255) not null default '',
  created_at timestamp not null default now(),
  update_at timestamp not null default now(),
  complete_at timestamp not null default now(),
  primary key (id),
);