module.exports = {
    create,
    list,
    find,
    changeContent,
    remove,
    markComplete,
    markIncomplete
}

async function create(db, todo) {
    await db.execute(`insert into todos (todo_message) values (?)`, 
    [todo.todo])
    return;
}

async function list(db) {
    let row = await db.execute ('select * from todos');
    return row[0];
}

async function find(db, id) {
     let results = await db.execute('select * from todos where id = ?',[id]);
     return results[0];
}

async function changeContent(db, id, todo) {
  await db.execute(`update todos set todo_message = ?,update_at = now() where id = ?`,
    [todo.todo,id])
    return;
 
}

async function remove(db, id) {
    await db.execute(`delete from todos where id = ? `, 
    [id])
    return;
}

async function markComplete(db, id) {
    await db.execute(`update todos set status_complete = true,update_at = now() where id = ? `,
    [id])
    return;
}

async function markIncomplete(db, id) {
  await db.execute(`update todos set status_complete = false,update_at = now()  where id = ? `,
  [id])
  return;
}