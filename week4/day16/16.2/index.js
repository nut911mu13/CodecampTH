"use strict";
const Koa = require('koa')
const Router = require('koa-router')
//const bodyParser = require('koa-bodyparser')
//const mysql2 = require('mysql2/promise')

// const dbConfig = require('./config/db')

// const makeTodoCtrl = require('./ctrl/todo')
// const todoRepo = require('./repo/todo')

// const pool = mysql2.createPool(dbConfig)

// const todoCtrl = makeTodoCtrl(pool, todoRepo)

const router = new Router()
// Auth
router.post('/auth/signup',(ctx) => {ctx.body = {}; })
router.post('/auth/signin', (ctx) => { ctx.body = {}; })
router.get('/auth/signout', (ctx) => { ctx.body = {}; })
router.post('/auth/verify', (ctx) => { ctx.body = {}; })

//upload
router.post('/upload', (ctx) => { ctx.body = {}; })


//user
router.patch('/user/:id', (ctx) => { ctx.body = {}; })
router.put('/user/:id/follow', (ctx) => { ctx.body = {}; })
router.delete('/user/:id/follow', (ctx) => { ctx.body = {}; })
router.get('/user/:id/follow', (ctx) => { ctx.body = {}; })
router.get('/user/:id/followed', (ctx) => { ctx.body = {}; })
    
//Tweet
router.get('/tweet', (ctx) => { ctx.body = {}; })
router.post('/tweet', (ctx) => { ctx.body = {}; })
router.put('/tweet/:id/like', (ctx) => { ctx.body = {}; })
router.delete('/tweet/:id/like', (ctx) => { ctx.body = {}; })
router.post('/tweet/:id/retweet', (ctx) => { ctx.body = {}; })
router.put('/tweet/:id/vote/:voted', (ctx) => { ctx.body = {}; })
router.post('/tweet/:id/reply', (ctx) => { ctx.body = {}; })

//Notification
router.get('/get/notification', (ctx) => { ctx.body = {}; })

//Direct Message
router.get('/message', (ctx) => { ctx.body = {}; })
router.get('/message/:userId', (ctx) => { ctx.body = {}; })
router.post('/message/:userId', (ctx) => { ctx.body = {}; })


async function requestLogger(ctx, next) {
    console.log(`${ctx.method} ${ctx.path}`)
    await next()
}
    

const app = new Koa()

//app.use(bodyParser())
app.use(requestLogger)
app.use(router.routes())
app.listen(3000)







