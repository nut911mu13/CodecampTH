const Koa = require('koa')
const multer = require('koa-multer')
const upload = multer({ dest: 'upload/' })
const app = new Koa()
app.use(async (ctx) => {
    await upload.single('file')(ctx)
    ctx.body = ctx.req.file.path
})
app.listen(3000)