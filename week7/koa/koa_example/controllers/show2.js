const userModel = require('../models/user.js');

module.exports = {
    async showTwoUser2 (ctx) {
        console.log(ctx.myVariable);
        let rows = await userModel.getUserById(ctx.params.id);
        let rows2 = await userModel.getUserById(ctx.params.id2);
        let rows3 = await ctx.db.query('SELECT * FROM users WHERE id = 3');
        console.log(rows)

        if (ctx.session.count == undefined)
            ctx.session.count = 0;
        else
            ctx.session.count++;
    
        await ctx.render("show", {
            "username" : rows[0].username,
            "mobile_no" : rows[0].mobile_no,
            "username2" : rows2[0].username,
            "count" : ctx.session.count
        });
    },
    async showTwoUser3 (ctx) {
        let rows = await db.query("SELECT * FROM users WHERE id = ?", [ctx.params.id]);
        let rows2 = await db.query("SELECT * FROM users WHERE id = ?", [ctx.params.id2]);
        
        if (ctx.session.count == undefined)
            ctx.session.count = 0;
        else
            ctx.session.count++;
    
        await ctx.render("show", {
            "username" : rows[0].username,
            "mobile_no" : rows[0].mobile_no,
            "username2" : rows2[0].username,
            "count" : ctx.session.count
        });
    },
    async showTwoUser4 (ctx) {
        let rows = await db.query("SELECT * FROM users WHERE id = ?", [ctx.params.id]);
        let rows2 = await db.query("SELECT * FROM users WHERE id = ?", [ctx.params.id2]);
        
        if (ctx.session.count == undefined)
            ctx.session.count = 0;
        else
            ctx.session.count++;
    
        await ctx.render("show", {
            "username" : rows[0].username,
            "mobile_no" : rows[0].mobile_no,
            "username2" : rows2[0].username,
            "count" : ctx.session.count
        });
    },
    async showTwoUser5 (ctx) {
        let rows = await db.query("SELECT * FROM users WHERE id = ?", [ctx.params.id]);
        let rows2 = await db.query("SELECT * FROM users WHERE id = ?", [ctx.params.id2]);
        
        if (ctx.session.count == undefined)
            ctx.session.count = 0;
        else
            ctx.session.count++;
    
        await ctx.render("show", {
            "username" : rows[0].username,
            "mobile_no" : rows[0].mobile_no,
            "username2" : rows2[0].username,
            "count" : ctx.session.count
        });
    },
    async showPost (ctx) {
        await ctx.render("show", {
            "username" : 'username',
            "mobile_no" : 'mobile_no',
            "username2" : 'username2',
            "postId" : ctx.request.body.postId,
            "count" : ctx.session.count
        });
    },
    async form (ctx) {
        await ctx.render('form');
    }
}