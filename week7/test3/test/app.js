const Koa = require('koa');
const render = require('koa-ejs');
const path = require('path');

const app = new Koa();
render(app, {
    root: path.join(__dirname, 'view'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});

app.use(async(ctx) => {
    await ctx.render('worldhello');
});


app.listen(3000);