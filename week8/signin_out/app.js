const Koa = require("Koa");
const Router = require("koa-router");
const session = require("koa-session");
const render = require("koa-ejs");
const path = require("path");
const app = new Koa();
const router = new Router();
const formidable = require("koa2-formidable");
const userController = require("./controller/user");
const userModel = require("./model/user_model");

const baseUrl = 'http://localhost:3000/'


render(app, {
    root: path.join(__dirname, 'view'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: false
});

app.keys = ['asdf;lkdsa naksldflkdsajflkdsa'];
app.use(formidable());

router
    .get("/", async ctx => {
        ctx.body = "homepage";
    })
    .get("/auth/home", userController.home)
    .get("/auth/signup", userController.signup)
    .post("/auth/signup_completed", userController.signupCompleted)
    .post("/auth/signup_completed_ajax", userController.signupCompletedAjax)
    .get("/auth/signin", userController.signin)
    .post("/auth/signin_completed", userController.signinCompleted)
    .get("/auth/signout", userController.signout)
    .get("/auth/user_profile", userController.UserProfile);


const sessionStore = {};

const CONFIG = {
    key: 'koa:sess',
    /** (string) cookie key (default is koa:sess) */
    /** (number || 'session') maxAge in ms (default is 1 days) */
    /** 'session' will result in a cookie that expires when session/browser is closed */
    /** Warning: If a session cookie is stolen, this cookie will never expire */
    maxAge: 60 * 60 * 1000,
    overwrite: true,
    /** (boolean) can overwrite or not (default true) */
    httpOnly: true,
    /** (boolean) httpOnly or not (default true) */
    store: {
        get(key, maxAge, { rolling }) {
            return sessionStore[key];
        },
        set(key, sess, maxAge, { rolling, changed }) {
            sessionStore[key] = sess;
        },
        destroy(key) {

        }
    }
};

app.use(session(CONFIG, app));
app.use(async(ctx, next) => {
    console.log("ctx.path=>", ctx.path);


    if (ctx.path == '/auth/signup') {
        console.log('auth/signup')
        await next();
        return;
    }
    if (ctx.path == '/auth/signup_completed_ajax') {
        console.log("auth/signup_completed_ajax");
        await next();
        return;
    }

    if (ctx.path == '/auth/signin') {
        console.log('auth/signin')
        await next();
        return;
    }
    if (ctx.path == '/auth/signin_completed') {
        console.log("auth/signin_completed");
        await next();
        return;
    }
    //await next()
    if (!ctx.session || (ctx.session && !ctx.session.userId)) {
        console.log("true ctx.session.userId=>", ctx.session.userId);
        await ctx.render('signin', {
            baseUrl: baseUrl
        });
    } else {
        console.log("false ctx.session.userId=>", ctx.session.userId);
    }

    if (ctx.path == '/auth/user_profile') {
        console.log('auth/user_profile')
        await next();
        return;
    }

    if (ctx.path == '/auth/signout') {
        console.log('auth/signout')
        await next();
        return;
    }

});

app
    .use(router.routes())
    .use(router.allowedMethods());

app.listen(3000);

// function signup(ctx) {
//     ctx.body = 'signup'
// }

// function signin(ctx) {
//     ctx.body = 'signin'