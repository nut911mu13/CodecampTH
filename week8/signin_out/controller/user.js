const userModel = require('../model/user_model');

module.exports = {
    async home(ctx) {
        await ctx.render("home", {
            baseUrl: "http://localhost:3000/auth/home"
        });
    },

    async signup(ctx) {
        await ctx.render("signup", {
            baseUrl: "http://localhost:3000/auth/signup"
        });
    },
    async signupCompleted(ctx) {
        let emailExisted = false;
        let usernameExisted = false;
        let emailExistedResult = await userModel.isEmailExisted(
            ctx.request.body.email
        );
        if (emailExistedResult) emailExisted = true;

        let usernameExistedResult = await userModel.isUsernameExisted(
            ctx.request.body.username
        );
        if (usernameExistedResult) usernameExisted = true;

        let userId;
        if (!emailExisted && !usernameExisted)
            userId = await userModel.insertNewUser(
                ctx.request.body.username,
                ctx.request.body.password,
                ctx.request.body.email
            );

        //console.log('3456');
        await ctx.render("signup_completed", {
            username: ctx.request.body.username,
            email: ctx.request.body.email,
            emailExisted: emailExisted,
            usernameExisted: usernameExisted,
            userId: userId
        });
    },
    async signupCompletedAjax(ctx) {
        console.log("adasd");
        let emailExisted = false;
        let usernameExisted = false;
        let emailExistedResult = await userModel.isEmailExisted(
            ctx.request.body.email
        );
        if (emailExistedResult) emailExisted = true;

        let usernameExistedResult = await userModel.isUsernameExisted(
            ctx.request.body.username
        );
        if (usernameExistedResult) usernameExisted = true;

        let userId;
        if (!emailExisted && !usernameExisted)
            userId = await userModel.insertNewUser(
                ctx.request.body.username,
                ctx.request.body.password,
                ctx.request.body.email
            );

        ctx.body = {
            emailExisted: emailExisted,
            usernameExisted: usernameExisted
        };
    },
    async signin(ctx) {
        await ctx.render("signin", {
            baseUrl: "http://localhost:3000/auth/signin"
        });
    },
    async signinCompleted(ctx) {
        let usernameExisted = false;
        let passwordMatched = false;
        // console.log(ctx.request.body);
        let usernameExistedResult = await userModel.isUsernameExisted(
            ctx.request.body.username
        );
        //console.log("usernameExistedResult=>", usernameExistedResult);
        //console.log(ctx.request.body.username);
        if (usernameExistedResult) {
            usernameExisted = true;
            if (
                await userModel.isPasswordMatched(
                    ctx.request.body.username,
                    ctx.request.body.password
                )
            )
                passwordMatched = true;
        }
        //console.log("passwordMatched=>", passwordMatched);
        if (passwordMatched) {
            let userRow = await userModel.getUserInfoByUsername(
                ctx.request.body.username
            );
            ctx.session.userId = userRow.id;
            console.log(ctx.session.userId);
            await ctx.render("signin_completed", {
                username: ctx.request.body.username
            });
        } else {
            await ctx.render("signin_failed", {
                usernameExisted: usernameExisted,
                passwordMatched: passwordMatched
            });
        }
    },
    async UserProfile(ctx) {
        const userRow = await userModel.getUserInfoByUserId(ctx.session.userId);
        //const userRow = await userModel.getUserInfoByUserId(3);
        console.log(ctx.session);
        await ctx.render("user_profile", {
            userRow: userRow
        });
    },
    async signout(ctx) {
        console.log("signout");
        ctx.session = null;
        ctx.body = "Signed out!!";
    }
    // async admin(ctx) {
    //     await ctx.render('admin');
    // }
};