
const mysql = require('mysql2/promise')

const pool = mysql.createPool({
    host: 'localhost',
    port: '8889',
    user: 'root',
    password: 'root',
    database: 'db1'
});

    (async function () {
        const db = await pool.getConnection()

        await db.beginTransaction()
        try {
            const User = require('./model/user')(db)
            const user1 = await User.find(1)
            const user1 = await User.find(1)
            user1.firstName = "tester"
            user1.save()
            const user2 = await User.find(2)
            user2.remove()

            const user3 = await User.findAll()
            console.log(user3);  //print findAll
           
            const user4 = await User.findByUsername('john')
            user4.firstName = "somchai"
            user4.save()
            console.log(user4); // print findByUsername
          
            

        } catch (err) {
            console.log(err)
            await db.rollback()
        }
        await db.release()
    })().then(
        () => { },
        (err) => { console.log(err) }
        )
