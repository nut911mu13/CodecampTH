//const User = require('./model/user')(db)
class User {
    constructor() {
        this.id
        this.firstName 
        this.lastName 
    }
            createEntity(row) {
            return {
                id: row.id,
                firstName: row.first_name,
                lastName: row.last_name
            }
        }       
    
}

module.exports = function (db) {
    return {
        async find(db,id) {
        //async find(id) {
            const [rows] = await db.execute('select id, first_name, last_name from users where id = ?', [id])
            //return User.createEntity(rows[0])
            return new User().createEntity(rows[0])
            //return new User(db, rows[0])
        },
        async findAll(db) {
            const [rows] = await db.execute('select id,username,first_name, last_name from users')
            //console.log(rows)
            //return rows.map(new User().createEntity(rows[0]))
            //return rows.map((row) => new User.(db, row))
            return rows.map((row) => new User().createEntity(row))
        },
        async findByUsername(db,username) {
            //const [rows] = await db.execute('select id, username, first_name, last_name from users')
            const [rows] = await db.execute('select id, username,first_name, last_name from users where username = ?', [username])
            return new User().createEntity(rows[0])
            //return new User(db, rows[0])
        },
        async save(db, user) {
            if (!user.id) {
                //if (!this.id) {
                const result = await db.execute('insert into users (first_name, last_name) values (?, ?)',
                    //const result = this._db.execute('insert into users (first_name, last_name) values (?, ?)',
                    [user.firstName, user.lastName])
                //[this.firstName, this.lastName])
                user.id = result.insertId
                return
            }
            return db.execute('update users set first_name = ?,last_name = ? where id = ?', [user.firstName, user.lastName, user.id])
            //return this._db.execute('update users set first_name = ?,last_name = ? where id = ?', [this.firstName, this.lastName, this.id])
        },

        async remove(db, id) {
                if (!user.id) {
                    //if (!this.id) {
                    const result = user.db.execute('insert into users (first_name, last_name) values (?, ?)', [user.firstName, user.lastName])
                    //const result = this._db.execute('insert into users (first_name, last_name) values (?, ?)', [this.firstName, this.lastName])
                    user.id = result.insertId
                    //this.id = result.insertId
                    return
                }
                {
                    //return this._db.execute('delete from users where id = ?', [this.id])
                    return db.execute('delete from users where id = ?', [id])
                }

            }

    }

}
 