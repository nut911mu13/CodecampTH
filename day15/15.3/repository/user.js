module.exports = {
    create,
    changePhoto,
    changeCover,
    changeTheme,
    Location,
    changeBio,
    Birthdate_d,
    Birthdate_m,
    Birthdate_y,
    Show_dm,
    Show_y,
    follow,
    unfollow
}

async function create(db, user) {
    const result = await db.execute(`
    insert into users (
      username, email, password, name, status
    ) values (
      ?, ?, ?, ?, ?
    )
  `, [
            user.username, user.email, user.password,
            user.name, 0
        ])
    return result[0].insertId
}

async function changePhoto(db, userId, photoUrl) {
    await db.execute(`
    update users set
      photo = ?
    where id = ?
  `, [photoUrl, userId])
}

async function changeCover(db, userId, photoUrl) {
    await db.execute(`
    update users set
      cover = ?
    where id = ?
  `, [photoUrl, userId])
}

async function changeTheme(db, userId, photoUrl) {
  await db.execute(`
    update users set
      theme = ?
    where id = ?
  `, [photoUrl, userId])
}

async function Location(db, userId, locationUrl) {
  await db.execute(`
    update users set
       location = ?
    where id = ?
  `, [locationUrl, userId])
}

async function changeBio(db, userId, bio) {
  await db.execute(`
     update users set
      bio = ?
    where id = ?
  `, [userBio, userId])
}

async function Birthdate_d(db, userId) {
  await db.execute(`
     insert into users (birth_date_d)
      values (?)
  `, [user.birth_date_d])
  return;
}

async function Birthdate_m(db, userId) {
  await db.execute(`
     insert into users (birth_date_m)
      values (?)
  `, [user.birth_date_m])
  return;
}

async function Birthdate_y(db, userId) {
  await db.execute(`
     insert into users (birth_date_y)
      values (?)
  `, [user.birth_date_y])
  return;
}

async function Show_dm(db, userId) {
  await db.execute ('select birth_date_d, birth_date_m from user')
  return;
}

async function Show_y(db, userId) {
  await db.execute('select birth_date_y from user')
  return;
}


async function follow(db, followerId, followingId) {
    await db.execute(`
    insert into follows (
      follower_id, following_id
    ) values (
      ?, ?
    )
  `, [followerId, followingId])
}

async function unfollow(db, followerId, followingId) {
    await db.execute(`
    delete from follows
    where follower_id = ? and following_id = ?
  `, [followerId, followingId])
}
    
