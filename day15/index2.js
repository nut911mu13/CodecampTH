const mysql = require('mysql2/promise')
const User = require('./repository/user')

const pool = mysql.createPool({
    host: 'localhost',
    port: '8889',
    user: 'root',
    password: 'root',
    database: 'db1'
});

    (async function () {
        const db = await pool.getConnection()
        await db.beginTransaction()
        try {
            //const User = require('./model/user')(db)
            const User = require('./repository/user')(db)
            const user1 = await User.find(db,1)
            //const user1 = await User.find(1)
            user1.firstName = "tester"
            //console.log(user1);
            await User.save(db,user1)
            //user1.save()
            const user2 = await User.find(db,2)
            //const user2 = await User.find(2)
            //await User2.remove(db,user2)
    

            const user3 = await User.findAll(db)
            //const user3 = await User.findAll()
            console.log(user3);  //print findAll

            
            const user4 = await User.findByUsername(db,'john')
            //const user4 = await User.findByUsername('john')
            user4.firstName = "somchai"
            //await User.save(db,username)
            
            console.log(user4); // print findByUsername
          
            
            await db.commit()

            // find(id)
            const [rows] = await db.execute('select id, first_name, last_name from users where id = ?', [1])
            console.log(rows)
            

        } catch (err) {
            console.log(err)
            await db.rollback()
        }
        await db.release()
    })().then(
        () => { },
        (err) => { console.log(err) }
        )