• จากตัวอย่าง JOIN


• มีใครบ้างที่ไม่ได้สอนอะไรเลย

select i.id, i.name as instructor_name ,c.id, c.name as course_name
from courses as c
right join instructors as i on i.id = c.teach_by
where c.id is null
order by i.id;
mysql> select i.id, i.name as instructor_name ,c.id, c.name as course_name
    -> from courses as c
    -> right join instructors as i on i.id = c.teach_by
    -> where c.id is null
    -> order by i.id;
+----+------------------+------+-------------+
| id | instructor_name  | id   | course_name |
+----+------------------+------+-------------+
| 25 | Martin Scorsese  | NULL | NULL        |
| 26 | Bob Woofward     | NULL | NULL        |
| 27 | Ron Howard       | NULL | NULL        |
| 28 | Thomas Keller    | NULL | NULL        |
| 29 | Alice Waters     | NULL | NULL        |
| 30 | Helen Mirren     | NULL | NULL        |
| 31 | Armin Van Buuren | NULL | NULL        |
+----+------------------+------+-------------+
7 rows in set (0.00 sec)


• มีคอร์สไหนบ้าง ที่ไม่มีคนสอน
select c.id, c.name as course_name, i.name as instructor_name
from courses as c
left join instructors as i on i.id = c.teach_by
where i.id is null
order by c.id;

mysql> select c.id, c.name as course_name, i.name as instructor_name
    -> from courses as c
    -> left join instructors as i on i.id = c.teach_by
    -> where i.id is null
    -> order by c.id;
+----+-------------------------+-----------------+
| id | course_name             | instructor_name |
+----+-------------------------+-----------------+
| 25 | Database System Concept | NULL            |
| 26 | JavaScript for Beginner | NULL            |
| 27 | OWASP Top 10            | NULL            |
+----+-------------------------+-----------------+
3 rows in set (0.00 sec)









