// • เพิ่ม student ลงไป 10 คน
insert into students (id, name) values
    (101, 'Bojana Jankova'),
    (102, 'Michelle Salgado'),
    (103, 'Samita Thapa'),
    (104, 'Jieun Han'),
    (105, 'Aranzazu Torres'),
    (106, 'Jon Kaasa'),
    (107, 'Karina Flores'),
    (108, 'Steve Heim'),
    (109, 'Pamela Chubb'),
    (110, 'Daniel Lichty')
    ;

alter table students auto_increment = 111;
mysql> alter table students auto_increment = 111;
Query OK, 0 rows affected (0.02 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> select * from students;
+-----+------------------+---------------------+
| id  | name             | created_at          |
+-----+------------------+---------------------+
| 101 | Bojana Jankova   | 2018-01-09 13:03:27 |
| 102 | Michelle Salgado | 2018-01-09 13:03:27 |
| 103 | Samita Thapa     | 2018-01-09 13:03:27 |
| 104 | Jieun Han        | 2018-01-09 13:03:27 |
| 105 | Aranzazu Torres  | 2018-01-09 13:03:27 |
| 106 | Jon Kaasa        | 2018-01-09 13:03:27 |
| 107 | Karina Flores    | 2018-01-09 13:03:27 |
| 108 | Steve Heim       | 2018-01-09 13:03:27 |
| 109 | Pamela Chubb     | 2018-01-09 13:03:27 |
| 110 | Daniel Lichty    | 2018-01-09 13:03:27 |
+-----+------------------+---------------------+
10 rows in set (0.00 sec)


• ให้ student ลงเรียนแต่ละวิชาที่แตกต่างกัน (อาจจะซ้ำกันบ้าง)
insert into enrolls (student_id,course_id) values(101,10),(102,1),(103,15),(104,29),(105,20),(106,26),(107,15),(108,2),(109,24),
(110,26);




• มีคอร์สไหนบ้างที่มีคนเรียน (ห้ามแสดงชื่อคอร์สซ้ำ)
select distinct (courses.name),enrolls.course_id from courses 
inner join enrolls on courses.id = enrolls.course_id
order by courses.id;
mysql> select distinct (courses.name),enrolls.course_id from courses 
    -> inner join enrolls on courses.id = enrolls.course_id
    -> order by courses.id;
+-------------------------+-----------+
| name                    | course_id |
+-------------------------+-----------+
| Cooking                 |         1 |
| Acting                  |         2 |
| Conservation            |         5 |
| Design and Architecture |        10 |
| Film Scoring            |        15 |
| Screenwriting           |        20 |
| Photography             |        24 |
| JavaScript for Beginner |        26 |
+-------------------------+-----------+
8 rows in set (0.00 sec)


• มีคอร์สไหนบ้างที่ไม่มีคนเรียน
select courses.name from courses
left join enrolls on enrolls.course_id = courses.id
where enrolls.course_id is null
order by courses.id;
mysql> select courses.name from courses
    -> left join enrolls on enrolls.course_id = courses.id
    -> where enrolls.course_id is null
    -> order by courses.id;
+-------------------------------------+
| name                                |
+-------------------------------------+
| Chess                               |
| Writing                             |
| Tennis                              |
| The Art of Performance              |
| Writing #2                          |
| Building a Fashion Brand            |
| Singing                             |
| Jazz                                |
| Country Music                       |
| Fashion Design                      |
| Comedy                              |
| Writing for Television              |
| Filmmaking                          |
| Dramatic Writing                    |
| Electronic Music Production         |
| Cooking #2                          |
| Shooting, Ball Handler, and Scoring |
| Database System Concept             |
| OWASP Top 10                        |
+-------------------------------------+
19 rows in set (0.00 sec)

select courses.name,courses.id 
from courses 
left join enrolls on enrolls.course_id = courses.id
where enrolls.course_id is null
order by courses.id;
mysql> select courses.name,courses.id 
    -> from courses 
    -> left join enrolls on enrolls.course_id = courses.id
    -> where enrolls.course_id is null
    -> order by courses.id;
+-------------------------------------+----+
| name                                | id |
+-------------------------------------+----+
| Chess                               |  3 |
| Writing                             |  4 |
| Tennis                              |  6 |
| The Art of Performance              |  7 |
| Writing #2                          |  8 |
| Building a Fashion Brand            |  9 |
| Singing                             | 11 |
| Jazz                                | 12 |
| Country Music                       | 13 |
| Fashion Design                      | 14 |
| Comedy                              | 16 |
| Writing for Television              | 17 |
| Filmmaking                          | 18 |
| Dramatic Writing                    | 19 |
| Electronic Music Production         | 21 |
| Cooking #2                          | 22 |
| Shooting, Ball Handler, and Scoring | 23 |
| Database System Concept             | 25 |
| OWASP Top 10                        | 27 |
+-------------------------------------+----+
19 rows in set (0.00 sec)






