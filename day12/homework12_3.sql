• จากการบ้านที่ 2 ข้อ 1 ให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ) โดยที่ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น

select distinct (courses.name),enrolls.course_id,
i.name as instructor_name, 
courses.price from courses 
inner join enrolls on courses.id = enrolls.course_id
left join instructors as i on i.id = courses.teach_by
order by courses.id;

mysql> select distinct (courses.name),enrolls.course_id,
    -> i.name as instructor_name, 
    -> courses.price from courses 
    -> inner join enrolls on courses.id = enrolls.course_id
    -> left join instructors as i on i.id = courses.teach_by
    -> order by courses.id;
+-------------------------+-----------+-------------------+-------+
| name                    | course_id | instructor_name   | price |
+-------------------------+-----------+-------------------+-------+
| Cooking                 |         1 | Wolfgang Puck     |    90 |
| Acting                  |         2 | Samuel L. Jackson |    90 |
| Conservation            |         5 | Dr. Jane Goodall  |    90 |
| Design and Architecture |        10 | Frank Gehry       |    90 |
| Film Scoring            |        15 | Hans Zimmer       |    90 |
| Screenwriting           |        20 | Aaron Sorkin      |    90 |
| Photography             |        24 | Annie Leibovitz   |    90 |
| JavaScript for Beginner |        26 | NULL              |    20 |
+-------------------------+-----------+-------------------+-------+
8 rows in set (0.00 sec)



• จากการบ้านที่ 2 ข้อ 2 ให้หาว่าใครเป็นคนสอน และราคาเท่าไร (ห้ามแสดงคอร์สซ้ำ โดยที่ให้ใช้คำสั่งแค่ statement เดียวเท่านั้น
select courses.name,courses.id, 
i.name as instructor_name, 
courses.price from courses 
left join enrolls on enrolls.course_id = courses.id
left join instructors as i on i.id = courses.teach_by
where enrolls.course_id is null
order by courses.id;
mysql> select courses.name,courses.id, 
    -> i.name as instructor_name, 
    -> courses.price from courses 
    -> left join enrolls on enrolls.course_id = courses.id
    -> left join instructors as i on i.id = courses.teach_by
    -> where enrolls.course_id is null
    -> order by courses.id;
+-------------------------------------+----+-----------------------+-------+
| name                                | id | instructor_name       | price |
+-------------------------------------+----+-----------------------+-------+
| Chess                               |  3 | Garry Kasparov        |    90 |
| Writing                             |  4 | Judy Blume            |    90 |
| Tennis                              |  6 | Serena Williams       |    90 |
| The Art of Performance              |  7 | Usher                 |    90 |
| Writing #2                          |  8 | James Patterson       |    90 |
| Building a Fashion Brand            |  9 | Diane Von Furstenberg |    90 |
| Singing                             | 11 | Christina Aguilera    |    90 |
| Jazz                                | 12 | Herbie Hancock        |    90 |
| Country Music                       | 13 | Reba Mcentire         |    90 |
| Fashion Design                      | 14 | Marc Jacobs           |    90 |
| Comedy                              | 16 | Steve Martin          |    90 |
| Writing for Television              | 17 | Shonda Rhimes         |    90 |
| Filmmaking                          | 18 | Werner Herzog         |    90 |
| Dramatic Writing                    | 19 | David Mamet           |    90 |
| Electronic Music Production         | 21 | Deadmau5              |    90 |
| Cooking #2                          | 22 | Gordon Ramsay         |    90 |
| Shooting, Ball Handler, and Scoring | 23 | Stephen Curry         |    90 |
| Database System Concept             | 25 | NULL                  |    30 |
| OWASP Top 10                        | 27 | NULL                  |    75 |
+-------------------------------------+----+-----------------------+-------+
19 rows in set (0.00 sec)



• จากการบ้านที่ 3 ข้อ 2 ให้เอามาเฉพาะคอร์สที่มีคนสอน

select courses.name,courses.id, 
i.name as instructor_name, 
courses.price from courses 
left join enrolls on enrolls.course_id = courses.id
inner join instructors as i on i.id = courses.teach_by
where enrolls.course_id is null 
order by courses.id;

mysql> select courses.name,courses.id, 
    -> i.name as instructor_name, 
    -> courses.price from courses 
    -> left join enrolls on enrolls.course_id = courses.id
    -> inner join instructors as i on i.id = courses.teach_by
    -> where enrolls.course_id is null 
    -> order by courses.id;
+-------------------------------------+----+-----------------------+-------+
| name                                | id | instructor_name       | price |
+-------------------------------------+----+-----------------------+-------+
| Chess                               |  3 | Garry Kasparov        |    90 |
| Writing                             |  4 | Judy Blume            |    90 |
| Tennis                              |  6 | Serena Williams       |    90 |
| The Art of Performance              |  7 | Usher                 |    90 |
| Writing #2                          |  8 | James Patterson       |    90 |
| Building a Fashion Brand            |  9 | Diane Von Furstenberg |    90 |
| Singing                             | 11 | Christina Aguilera    |    90 |
| Jazz                                | 12 | Herbie Hancock        |    90 |
| Country Music                       | 13 | Reba Mcentire         |    90 |
| Fashion Design                      | 14 | Marc Jacobs           |    90 |
| Comedy                              | 16 | Steve Martin          |    90 |
| Writing for Television              | 17 | Shonda Rhimes         |    90 |
| Filmmaking                          | 18 | Werner Herzog         |    90 |
| Dramatic Writing                    | 19 | David Mamet           |    90 |
| Electronic Music Production         | 21 | Deadmau5              |    90 |
| Cooking #2                          | 22 | Gordon Ramsay         |    90 |
| Shooting, Ball Handler, and Scoring | 23 | Stephen Curry         |    90 |
+-------------------------------------+----+-----------------------+-------+
17 rows in set (0.00 sec)



