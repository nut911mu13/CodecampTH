สร้าง database สำหรับร้านหนังสือ
mysql>create database Bookshop;
mysql>show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| Bookshop           |
| codecamp           |
| db1                |
| mysql              |
| performance_schema |
+--------------------+
6 rows in set (0.00 sec)

สร้าง  table ดังนี้
    พนักงาน (id,ชื่อ ,นามสกุล , อายุ, วันเวลาที่เพิ่มข้อมูล )
    create table employees ( id int auto_increment,firstname varchar(50),lastname varchar(50),age int,created_at timestamp default now(),primary key (id) );

+ Options
Tables_in_bookshop
employees

mysql> describe employees;
+------------+-------------+------+-----+-------------------+----------------+
| Field      | Type        | Null | Key | Default           | Extra          |
+------------+-------------+------+-----+-------------------+----------------+
| id         | int(11)     | NO   | PRI | NULL              | auto_increment |
| firstname  | varchar(50) | YES  |     | NULL              |                |
| lastname   | varchar(50) | YES  |     | NULL              |                |
| age        | int(11)     | YES  |     | NULL              |                |
| created_at | timestamp   | NO   |     | CURRENT_TIMESTAMP |                |
+------------+-------------+------+-----+-------------------+----------------+
5 rows in set (0.01 sec)

    หนังสือ (ISBN , ชื่อหนังสือ ,ราคา ,วันเวลาที่เพิ่มข้อมูล)
 create table books ( ISBN varchar (25),bookname varchar(120),prices int,created_at_books timestamp default now(),primary key (ISBN) );   
  +------------------+--------------+------+-----+-------------------+-------+
| Field            | Type         | Null | Key | Default           | Extra |
+------------------+--------------+------+-----+-------------------+-------+
| ISBN             | varchar(25)  | NO   | PRI |                   |       |
| bookname         | varchar(120) | YES  |     | NULL              |       |
| prices           | int(11)      | YES  |     | NULL              |       |
| created_at_books | timestamp    | NO   |     | CURRENT_TIMESTAMP |       |
+------------------+--------------+------+-----+-------------------+-------+
4 rows in set (0.01 sec)   


    รายการขาย (ISBN หนังสือ , พนักงานที่ขาย,ราคาที่ขาย,จำนวนเล่ม,วันเวลาที่ขาย)
create table SalesItem ( ISBNBook varchar (25),SalesPerson varchar(120),SalesPrices int(15),Quantity int (20),created_at_sales timestamp default now()); 
mysql> describe SalesItem;
mysql> describe SalesItem;
+------------------+--------------+------+-----+-------------------+-------+
| Field            | Type         | Null | Key | Default           | Extra |
+------------------+--------------+------+-----+-------------------+-------+
| ISBNBook         | varchar(25)  | YES  |     | NULL              |       |
| SalesPerson      | varchar(120) | YES  |     | NULL              |       |
| SalesPrices      | int(15)      | YES  |     | NULL              |       |
| Quantity         | int(20)      | YES  |     | NULL              |       |
| created_at_sales | timestamp    | NO   |     | CURRENT_TIMESTAMP |       |
+------------------+--------------+------+-----+-------------------+-------+
5 rows in set (0.01 sec)