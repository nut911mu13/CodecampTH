• เพิ่มพนักงานเข้าไป 10 คน โดยข้อมูลห้ามซ้ำกัน
INSERT INTO employees (firstname, lastname, age)
VALUES ('Sykes', 'Cotton',19),('Horton', 'Herring',23),('Vaughn', 'Clemons',25),('Wheeler', 'Kelley',35),
('Katie', 'Simon',32),('Lauren', 'Munoz',40),('Michael', 'Woods',18),('Pearson', 'Vega',27),
('Cummings', 'Hardy',30),('Marsh', 'Perkins',25) ;

mysql> select * from employees;
+----+-----------+----------+------+---------------------+
| id | firstname | lastname | age  | created_at          |
+----+-----------+----------+------+---------------------+
|  1 | Sykes     | Cotton   |   19 | 2018-01-08 16:08:51 |
|  2 | Horton    | Herring  |   23 | 2018-01-08 16:08:51 |
|  3 | Vaughn    | Clemons  |   25 | 2018-01-08 16:08:51 |
|  4 | Wheeler   | Kelley   |   35 | 2018-01-08 16:08:51 |
|  5 | Katie     | Simon    |   32 | 2018-01-08 16:08:51 |
|  6 | Lauren    | Munoz    |   40 | 2018-01-08 16:08:51 |
|  7 | Michael   | Woods    |   18 | 2018-01-08 16:08:51 |
|  8 | Pearson   | Vega     |   27 | 2018-01-08 16:08:51 |
|  9 | Cummings  | Hardy    |   30 | 2018-01-08 16:08:51 |
| 10 | Marsh     | Perkins  |   25 | 2018-01-08 16:08:51 |
+----+-----------+----------+------+---------------------+
10 rows in set (0.00 sec)

• พนักงานคนที่ 5 ลาออก ลบข้อมูลคนที่ 5 ออก

delete from employees where id = 5;

mysql> select * from employees;
+----+-----------+----------+------+---------------------+
| id | firstname | lastname | age  | created_at          |
+----+-----------+----------+------+---------------------+
|  1 | Sykes     | Cotton   |   19 | 2018-01-08 16:08:51 |
|  2 | Horton    | Herring  |   23 | 2018-01-08 16:08:51 |
|  3 | Vaughn    | Clemons  |   25 | 2018-01-08 16:08:51 |
|  4 | Wheeler   | Kelley   |   35 | 2018-01-08 16:08:51 |
|  6 | Lauren    | Munoz    |   40 | 2018-01-08 16:08:51 |
|  7 | Michael   | Woods    |   18 | 2018-01-08 16:08:51 |
|  8 | Pearson   | Vega     |   27 | 2018-01-08 16:08:51 |
|  9 | Cummings  | Hardy    |   30 | 2018-01-08 16:08:51 |
| 10 | Marsh     | Perkins  |   25 | 2018-01-08 16:08:51 |
+----+-----------+----------+------+---------------------+

• เจ้าของร้านอยากเก็บที่อยู่พนักงานเพิ่ม ใส่ที่อยู่ให้พนักงานแต่ละคน คนไหนไม่รู้ให้เป็น NULL
alter table employees add column address varchar(255);
mysql> select * from employees;
+----+-----------+----------+------+---------------------+---------+
| id | firstname | lastname | age  | created_at          | address |
+----+-----------+----------+------+---------------------+---------+
|  1 | Sykes     | Cotton   |   19 | 2018-01-08 16:08:51 | NULL    |
|  2 | Horton    | Herring  |   23 | 2018-01-08 16:08:51 | NULL    |
|  3 | Vaughn    | Clemons  |   25 | 2018-01-08 16:08:51 | NULL    |
|  4 | Wheeler   | Kelley   |   35 | 2018-01-08 16:08:51 | NULL    |
|  6 | Lauren    | Munoz    |   40 | 2018-01-08 16:08:51 | NULL    |
|  7 | Michael   | Woods    |   18 | 2018-01-08 16:08:51 | NULL    |
|  8 | Pearson   | Vega     |   27 | 2018-01-08 16:08:51 | NULL    |
|  9 | Cummings  | Hardy    |   30 | 2018-01-08 16:08:51 | NULL    |
| 10 | Marsh     | Perkins  |   25 | 2018-01-08 16:08:51 | NULL    |
+----+-----------+----------+------+---------------------+---------+
9 rows in set (0.00 sec)

ใส่ที่อยู่ให้พนักงานแต่ละคน คนไหนไม่รู้ให้เป็น NULL
update employees set address = 'Seattle' where id = 1;
update employees set address = 'Norway' where id = 3;
update employees set address = 'England' where id = 7;
update employees set address = 'Sydney' where id = 9;
update employees set address = 'Finland' where id = 10;

mysql> select * from employees;
+----+-----------+----------+------+---------------------+---------+
| id | firstname | lastname | age  | created_at          | address |
+----+-----------+----------+------+---------------------+---------+
|  1 | Sykes     | Cotton   |   19 | 2018-01-08 16:08:51 | Seattle |
|  2 | Horton    | Herring  |   23 | 2018-01-08 16:08:51 | NULL    |
|  3 | Vaughn    | Clemons  |   25 | 2018-01-08 16:08:51 | Norway  |
|  4 | Wheeler   | Kelley   |   35 | 2018-01-08 16:08:51 | NULL    |
|  6 | Lauren    | Munoz    |   40 | 2018-01-08 16:08:51 | NULL    |
|  7 | Michael   | Woods    |   18 | 2018-01-08 16:08:51 | England |
|  8 | Pearson   | Vega     |   27 | 2018-01-08 16:08:51 | NULL    |
|  9 | Cummings  | Hardy    |   30 | 2018-01-08 16:08:51 | Sydney  |
| 10 | Marsh     | Perkins  |   25 | 2018-01-08 16:08:51 | Finland |
+----+-----------+----------+------+---------------------+---------+
9 rows in set (0.00 sec) 

• หาจำนวนพนักงานทั้งหมดในร้าน
select count(*) from employees;
mysql> select count(*) from employees;
+----------+
| count(*) |
+----------+
|        9 |
+----------+
1 row in set (0.00 sec)

• แสดงรายชื่อพนักงานที่อายุน้อยกว่า 20 ปี
select * from employees where age < 20 ;
mysql> select * from employees where age < 20 ;
+----+-----------+----------+------+---------------------+---------+
| id | firstname | lastname | age  | created_at          | address |
+----+-----------+----------+------+---------------------+---------+
|  1 | Sykes     | Cotton   |   19 | 2018-01-08 16:08:51 | Seattle |
|  7 | Michael   | Woods    |   18 | 2018-01-08 16:08:51 | England |
+----+-----------+----------+------+---------------------+---------+
2 rows in set (0.00 sec)