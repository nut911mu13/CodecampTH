const Koa = require('koa');//npm install koa
const app = new Koa();
const mysql = require('mysql2/promise');//npm install mysql2-promise

const bodyParser = require('koa-bodyparser');//npm install koa-bodyparser
const Router = require('koa-router');//npm install koa-router
const cors = require('@koa/cors');//npm install cors

let main = async ()=>{
let db = await mysql.createConnection({
    "localhost" : "127.0.0.1",
    "user" : "root",
    "database" : "tatododb"
    }
);

await db.execute('select * from tatodotb');

let router = new Router();
router.get('/list/todo', async (ctx)=>{
    let [result] = await db.execute(`select * from tatodotb;`);
    ctx.body = result;
})
.post('/add/todo', async (ctx)=>{
    let {content} = ctx.request.body;
    await db.execute(`INSERT INTO tatodotb (content, status, create_at) VALUES (?, ?, ?);`,[content, 1, Date.now()]);
})
// .post('/edit/todo', async (ctx)=>{
//     let {isbn, bookName, price, promoDate, imgPath} = ctx.request.body;
//     await db.execute(`UPDATE tatodotb SET bookname = ?, price = ?, promotion_date = ?, image = ? WHERE isbn = ?;`,[bookName, price, promoDate, imgPath, isbn]);
// })
.get('/delete/todo/:id', async (ctx)=>{
    let idtemp = ctx.params.id;
    //let {isbn, bookName, price, promoDate, imgPath} = ctx.request.body;
    await db.execute(`DELETE FROM tatodotb WHERE id = ?;`,[idtemp]);
})



app.use(cors());
app.use(bodyParser());
app.use(router.routes());

app.listen(4002);
}

main();