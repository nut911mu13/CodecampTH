import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
//import Hw1 from './Homework1';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
