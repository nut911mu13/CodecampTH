import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import { Button } from 'antd';
import 'antd/dist/antd.css';


class App extends Component {
    render() {
        return ( 
        <div className = "App" >
            <header className = "App-header" >
            <h1 > Hello World </h1>  
                    <div>
                        <Button type="primary">Primary</Button>
                        <Button>Default</Button>
                        <Button type="dashed">Dashed</Button>
                        <Button type="danger">Danger</Button>
                    </div>
            </header >

            </div>
        );
    }
}

export default App;