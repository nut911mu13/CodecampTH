import React, { Component } from 'react'
import { Layout, Form, Input, Icon, Row, Col, Button, Card, List ,Avatar} from 'antd'
import logo from './logo.svg'
import './App.css'


class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputText: '',
      listItem: []
    }

    this.handleChangeText = this.handleChangeText.bind(this);

  }

  deleteListAtIndex = (index) => {
    // ไม่ควรทำเพราะเป็นการ Render ใหม่ทั้ง State ถ้ามีเยอะก็ฉิบหายยย สิครับ
    // this.state.listItem.splice(index, 1);
    // this.setState({});

    const result = this.state.listItem;
    result.splice(index, 1);
    this.setState({ listItem: result });
  }

  submitList = () => {
   // if (this.state.inputText === '') {
    this.setState({     
        listItem: this.state.listItem.concat([this.state.inputText]),
          inputText: ''
      })     
   // }
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({ inputText: event.target.value });
  }


  render () {

    const { Header, Footer, Sider, Content } = Layout;
    const Search = Input.Search;
    const FormItem = Form.Item;
    return (
      <Card className="cardcenter" style={{ width: 500 , backgroundColor : this.props.myColor }}>
        <h1> <Icon type="user" /> Chat Messenger</h1>

            
            <List
              bordered
              dataSource={this.state.listItem}
              renderItem={(item, index) => (
                <List.Item>
                  { index % 2 === 0 ?
                  <List.Item.Meta style={{textAlign: 'left'}}
                    title={
                    <a href="https://ant.design">
                        {<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                        {item}
                    </a>}
                  /> 
                  :
                    <List.Item.Meta style={{ textAlign: 'right' }}
                      title={
                        <a href="https://ant.design">
                          {<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                          {item}
                        </a>}
                    /> 
                   }
                </List.Item>
              )}
            />

            <div style={{ marginBottom:'10px'}}>
              <Input
            addonAfter={<Button type="primary" onClick={this.submitList}><Icon type="message" /></Button>}
                onChange={this.handleChangeText}
                value={this.state.inputText}
                onKeyPress={this.handleKeyPress}/>
            </div>


            {/*
              this.state.listItem.map((value, index) => {
                //console.log(index);
                return (
                  <h3 key={index + value}>{value}</h3>
                );
              })
            */}
            
        </Card>
    )
  }
}

export default App
