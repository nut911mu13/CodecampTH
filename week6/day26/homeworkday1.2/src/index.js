import React from 'react'
import ReactDOM from 'react-dom'
//import './index.css'
import './AppHW1_3.css'
// import App from './App'
import App from './AppHW1_3'

import registerServiceWorker from './registerServiceWorker'

ReactDOM.render(< App /> , document.getElementById('root'))
registerServiceWorker()
