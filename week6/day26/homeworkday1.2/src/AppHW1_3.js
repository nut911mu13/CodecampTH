import React, { Component } from 'react';
import { Row, Col,Icon } from 'antd';
import './AppHW1_3.css';

const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
class App extends Component{
    render (){
        return(
            
            <Row type="flex" justify="space-around" align="middle">

                <Col span={12} className="custom-card-left "> 
                    <DemoBox value={50}></DemoBox>
                    <div className="centered1 ">  
                    <p><h3> <Icon type="search"/> Follow for your interests </h3> </p>
                        <p> <h3> <Icon type="user" /> Hear what people are talking about. </h3></p>
                        <p> <h3> <Icon type="message" /> Join the conversation </h3></p>  
                    </div> 
                </Col>
            
                    
                    <Col span={12}>
                    <DemoBox value={50}></DemoBox>
                    <div className="custom-card-right">
                        

                    </div>
                    </Col>
            </Row>
            
        )
    }
}
export default App;