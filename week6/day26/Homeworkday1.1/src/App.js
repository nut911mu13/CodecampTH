import React, { Component } from 'react';
import logo from './logo.svg';
import { Form, Icon, Input, Button, Checkbox, Card } from 'antd';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    }

    this.handleChange = this.handleChange.bind(this);

  }

  handleChange(event) {
    this.setState({ username: event.target.value });
    console.log(this.state.username);
  }

  render() {

    const FormItem = Form.Item;

    return (
        <div className="App">

            <h1>Welcome to my application</h1>
            <Card title="SIGN IN" style={{ width: 300, margin: 'auto' }}>
              <Form className="login-form">
                <Form.Item>
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                placeholder="Username" value={this.state.username}
                onChange={this.handleChange}
              />
                </Form.Item>
                <Form.Item>
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                      Sign in
                    </Button>
                    <p>
                    <a className="login-form-forgot" href="">Forgot password</a>
                    </p>
              <hr></hr>
                </Form.Item>               
                <Form.Item>
                    <p>New User</p>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                      Sign up
                    </Button>
                </Form.Item>
              </Form>
            </Card>
        </div>
    );
  }
}

export default App;
