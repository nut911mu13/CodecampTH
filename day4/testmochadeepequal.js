let assert = require('assert');
describe('Array', function () {
    describe('#testObject()', function () {
        it('should be equal to this object structure', function () {
            let myVariable = { a: 1, b: { c: [1, 2, 3] } };
            assert.deepEqual(myVariable, { a: 1, b: { c: [1, 2, 3] } }, 'Obj myVariable');
        });
    });
});