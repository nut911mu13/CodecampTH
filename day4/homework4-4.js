let fs = require('fs')

fs.readFile(__dirname + '/homework1-4.json', 'utf8', function (err, data) {  //การหาdirectoryไม่เจอให้ใส่__dirname และ '/ตามด้วยชื่อไฟล์
    let dataJson = JSON.parse(data);
    const result = dataJson.filter(data => {
        return data.gender == "male" && data.friends.length >= 2
    })
    
    const resultMap = result.map(data => {
        let obj = {}
        obj.name = data.name
        obj.gender = data.gender
        obj.company = data.company
        obj.email = data.email
        obj.friends = data.friends
        let Sumbalance = data.balance.substr(1).replace(",", "")
        obj.balance = "$" + Sumbalance / 10
        return obj
    })    
    console.log(resultMap)
});

