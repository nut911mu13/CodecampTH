const koa = require("koa");
const render = require("koa-ejs");
const path = require("path");
const app = new koa();
//const {db} = require('./lib/db.js');
//const {routes} = require('./routes.js');
const str = require("./controller/routes.js")(app);
//const mysql = require('mysql2/promise');


render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
    debug: true
});


// get the client


/*app.use(async (ctx, next) => {
    try {
        // create the connection
        /*
        const connection = await mysql.createConnection(
            {
                host: 'localhost',
                port: '8889',
                user: 'root',
                password: 'root',
                database: 'codecamp'
            }
        );
        */
        // query database
     //   const [rows, fields] = await db.execute('SELECT * FROM user');
 
      //  await ctx.render('homework10_1', { message: rows });
     //   await next();
    //} catch (err) {
     //   ctx.status = 400
     //   ctx.body = `Uh-oh: ${err.message}`
    //}
//});


app.listen(3500);


