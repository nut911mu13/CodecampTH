const {db} = require("../lib/db.js");
class queryDB {
    async getUser() {
        let result = await db.execute("SELECT * FROM user");
        return result;
    }
}
module.exports.queryDB = new queryDB();