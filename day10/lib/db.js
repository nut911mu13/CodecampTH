const mysql = require('mysql2/promise');
const {dbconfig} = require("../config/database.js");

class Database {
    constructor() {
        this.connectDatabase();
    }
    async connectDatabase() {
        const mysql = require('mysql2/promise');
        // create the connection
        this.connection = await mysql.createConnection(
            {
                host: dbconfig.host ,
                port: dbconfig.port,
                user: dbconfig.user,
                password: dbconfig.password,
                database: dbconfig.database
            }
        );
    }
    async execute(sql) {
        try {
            //query database
            const [row,field] = await this.connection.execute(sql);
            //let result = await this.connection.execute(sql);
            
            return row;
        }
        catch (err) {
            console.log(err);
        }
    }
}

module.exports.db = new Database();