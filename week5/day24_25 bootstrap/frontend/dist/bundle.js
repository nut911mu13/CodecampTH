/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lib__ = __webpack_require__(1);

// import $ from 'jquery'
// import { fetchApi } from './middlewares'
// import bookPage from './views/bookPage.html'
// import bookItems from './views/bookItem.html'
// import { templateBinder } from './lib/template'

// function getdata() {
//     let url = 'https://localhost:4000/list'
//     fetch(url, {
//             method: 'GET', // or 'PUT'
//             // body: JSON.stringify(data),
//             headers: new Headers({
//                 'Content-Type': 'application/json'
//             })
//         }).then(res => res.json())
//         .catch(error => console.error('Error:', error))
//         .then(response => {
//             console.log('Success:', response)
//             $('#itemlist').html('')

//             // ApiListBook()
//             // $('#myModalAdd').modal('hide')
//         })
// }
// parent.appendChild(data)
// getdata()

// SAVE From
window.AddSave = function () {
    let Bookid = $('#AddBookId').val();
    let title = $('#AddTitle').val();
    let saleprice = $('#AddSalePrice').val();
    let promotiondate = $('#AddPromotionDate').val();
    let image = $('#AddImage').val();
    console.log('Addsave');
    // let parent = document.getElementById("itemlist")
    // let data = document.createElement("div")

    // data.innerHTML =
    let html = `<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" id="col-${Bookid}">
                    <div class="thumbnail">
                        <h3 id="title-${Bookid}">${title}</h3>
                        <img src="${image}" id="image-${Bookid}">
                        <div class="caption ">
                            <p>
                                <h4>Price : <span id="price-${Bookid}">${saleprice}</span>$ 
                                    <button type="button" class="btn btn-edit" onclick="editBook('${Bookid}')" data-toggle="modal" data-target="#myModalEdit" role="button ">
                                        Edit
                                        <span class="glyphicon glyphicon-pencil "></span>
                                    </button>
                                    <button type="button" class="btn btn-remove" onclick="deleted('${Bookid}')" role="button ">
                                        <span class="glyphicon glyphicon-trash "></span>
                                    </button>
                                </h4>
                            </p>
                        </div>
                    </div>`;

    // parent.appendChild(data)

    $('#itemlist').append(html);
    $('#myModalAdd').modal('hide');
};

window.editBook = function (id) {
    let bookName = document.getElementById('title-' + id).textContent;
    let bookImg = document.getElementById('image-' + id).currentSrc;
    let bookPrice = document.getElementById('price-' + id).textContent;
    console.log('editbook');
    $('#editbookid').val(id);
    $('#editbooktitle').val(bookName);
    $('#editbooksaleprice').val(bookPrice);
    $('#editImage').val(bookImg);
};

window.editcomplete = function () {
    console.log('editcomplete');
    let id = $('#editbookid').val();
    let bid = $('#BookId').val();
    let bname = $('#editbooktitle').val();
    let bimg = $('#editbookimage').val();
    let bprice = $('#editbooksaleprice').val();

    $('#title-' + id).html(bname);
    $('#price-' + id).html(bprice);
    $('#image-' + id).attr('src', bimg);

    $('#myModalEdit').modal('hide');
};

window.deleted = function (id) {
    console.log('delete');
    let PopUpAlert = confirm('Are you sure delete item');
    if (PopUpAlert == true) {
        $('#col-' + id).remove();
        // $('#row' + id).remove()
    } else {}
};
// console.log('abc')
// api Add book
window.ApiAddBook = function () {
    let Bookid = $('#AddBookId').val();
    let title = $('#AddTitle').val();
    let saleprice = $('#AddSalePrice').val();
    let promotiondate = $('#AddPromotionDate').val();
    let image = $('#AddImage').val();
    console.log('ApiAddBook');
    let url = 'http://localhost:4000/add/book';
    let data = {
        book_isbn: Bookid,
        book_name: title,
        book_price: saleprice,
        book_image: image,
        book_promotiondate: promotiondate
    };
    console.log('1234');

    fetch(url, {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(res => res.json()).catch(error => console.error('Error:', error)).then(response => {
        console.log('Success:', response);
        $('#itemlist').html('');
        ApiListBook();
        $('#myModalAdd').modal('hide');
    });
};

// api
window.ApiListBook = function () {
    console.log('ApiListBook');
    let url = 'http://localhost:4000/list/book';
    fetch(url, {
        method: 'get',
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(res => res.json()).then(function (data) {
        console.log('data:', data);
        data.forEach(element => {
            let html = `<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" id="col-${element.book_isbn}">
                    <div class="thumbnail">
                        <h3 id="title-${element.book_isbn}">${element.book_name}</h3>
                        <img src="${element.book_image}" id="image-${element.book_isbn}">
                        <div class="caption ">
                            <p>
                                <h4>Price : <span id="price-${element.book_isbn}">${element.book_price}</span>$ 
                                    <button type="button" class="btn btn-edit" onclick="editBook('${element.book_isbn}')" data-toggle="modal" data-target="#myModalEdit" role="button ">
                                        Edit
                                        <span class="glyphicon glyphicon-pencil "></span>
                                    </button>
                                    <button type="button" class="btn btn-remove" onclick="ApiDelBook('${element.book_isbn}')" role="button ">
                                        <span class="glyphicon glyphicon-trash "></span>
                                    </button>
                                </h4>
                            </p>
                        </div>
                    </div>`;
            $('#itemlist').append(html);
        });
    }).catch(error => console.error('Error:', error));
    // .then(response => {
    //     console.log('Success:', response)
    // })
};

ApiListBook();

window.ApiEditBook = function () {
    let Bookid = $('#editBookId').val();
    let bid = $('#BookId').val();
    let bname = $('#editebooktitle').val();
    let bimg = $('#editebookimage').val();
    let bprice = $('#editPrice').val();
    let bdate = $('#editebookpromotiondate').val();

    console.log('ApiEditBook');
    let url = 'http://localhost:4000/update/book';
    let data = {
        book_isbn: ebookId,
        book_name: ebookTitle,
        book_price: ebookPrice,
        book_image: ebookImage,
        book_promotiondate: ebookpromotionDate
    };
    console.log(data);

    $('#title-' + BookId).html($('#editebooktitle').val());
    $('#price-' + BookId).html($('#editebooksaleprice').val());
    $('#image-' + BookId).attr('src', $('#editebookimage').val());

    fetch(url, {
        method: 'put',
        body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(res => res.json()).catch(error => console.error('Error:', error)).then(response => {
        console.log('Success:', response);
        $('#itemlist').html('');
        ApiListBook();
        $('#myModalAdd').modal('hide');
    });

    $('#myModalEdit').modal('hide');
};

// API delete

window.ApiDelBook = function (id) {
    console.log('ApiDelBook');
    let PopUpAlert = confirm('Are you sure delete item');
    if (PopUpAlert == true) {
        let url = 'http://localhost:4000/delete/book';
        let data = { id: id };

        fetch(url, {
            method: 'post', // or 'PUT'
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }).then(res => res.json()).catch(error => console.error('Error:', error)).then(response => {
            console.log('Success:', response);
            $('#itemlist').html('');
            ApiListBook();
            $('#myModalAdd').modal('hide');
        });
        $('#col-' + id).remove();
        // $('#row-' + id).remove()
    } else {}
};

//   let ebookTitle = $('#editTitle').val()
//   let ebookPrice = $('#editPrice').val()
//   let ebookImage = $('#editImage').val()
//   let ebookPromotionDate = $('editebookpromotiondate').val()
//   let data = {
//     book_isbn: ebookId,
//     book_name: ebookTitle,
//     book_saleprice: ebookPrice,
//     book_image: ebookImage,
//     book_pomotiondate: ebookpromotionDate
//   }
//   console.log(data)

//   fetch(url, {
//     method: 'post',
//     body: JSON.stringify(data),
//     headers: new Headers({
//       'Content-Type': 'application/json'
//     })
//   }).then(res => res.json())
//     .catch(error => console.error('Error:', error))
//     .then(response => {
//       console.log('Success:', response)
//       $('#itemlist').html('')
//       ApiListBook()
//       $('#myModalAdd').modal('hide')
//     })

//   $('#myModalEdit').modal('hide')
// }

// API delete

// window.ApiDeletedBook = function(val) {
//         let PopUpAlert = confirm('Are you sure delete item')
//         if (PopUpAlert == true {
//                 let data = { val: BookId }

//                 fetch(url, {
//                         method: 'post', // or 'PUT'
//                         body: JSON.stringify(data),
//                         headers: new Headers({
//                             'Content-Type': 'application/json'
//                         })
//                     }).then(res => res.json())
//                     .catch(error => console.error('Error:', error))
//                     .then(response => {
//                         console.log('Success:', response)
//                         $('#itemlist').html('')
//                         ApiListBook()
//                         $('#myModalAdd').modal('hide')
//                     })

//                 $('#col-' + val).remove()
//             } else {

//             }
//         }

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__domUtil__ = __webpack_require__(2);
/* unused harmony reexport domUtil */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__bomUtil__ = __webpack_require__(3);
/* unused harmony reexport bomUtil */



/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class domUtil {
  constructor() {}
  static setText(id, text) {
    let elem = document.getElementById(id);
    elem.innerText = text;
  }

  static setHTML(id, html) {
    let elem = document.getElementById(id);
    elem.innerHTML = html;
  }

  static addItems(id, data) {
    let parent = document.getElementById(id);
    let ul = document.createElement("ul");
    ul.id = 'menu';

    data.forEach(a => {
      console.log('a', a);
      let li = document.createElement("li");
      //example 4-5
      // li.id = a.id
      // li.style.color = 'blue'
      // li.style.cursor = 'pointer'
      //example 4-6
      // li.addEventListener("click", () => alert(a.text))
      li.appendChild(document.createTextNode(a.text));
      ul.appendChild(li);
    });
    parent.appendChild(ul);
  }
  static editItem(id, text) {
    let elem = document.getElementById(id);
    elem.innerText = text;
  }

  static removeItem(removeId, parentId) {
    let elem = document.getElementById(removeId);
    let parent = document.getElementById(parentId);
    parent.removeChild(elem);
  }
}
/* unused harmony export domUtil */


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class bomUtil {
  static openWindow() {
    this.win = window.open("https://www.w3schools.com", "", "width=200,height=100");
  }
  static closeWindow() {
    if (this.win) {
      this.win.close();
    }
  }

}
/* unused harmony export bomUtil */


/***/ })
/******/ ]);