import { domUtil, bomUtil } from './lib'
// import $ from 'jquery'
// import { fetchApi } from './middlewares'
// import bookPage from './views/bookPage.html'
// import bookItems from './views/bookItem.html'
// import { templateBinder } from './lib/template'

// function getdata() {
//     let url = 'https://localhost:4000/list'
//     fetch(url, {
//             method: 'GET', // or 'PUT'
//             // body: JSON.stringify(data),
//             headers: new Headers({
//                 'Content-Type': 'application/json'
//             })
//         }).then(res => res.json())
//         .catch(error => console.error('Error:', error))
//         .then(response => {
//             console.log('Success:', response)
//             $('#itemlist').html('')

//             // ApiListBook()
//             // $('#myModalAdd').modal('hide')
//         })
// }
// parent.appendChild(data)
// getdata()

// SAVE From
window.AddSave = function() {
    let Bookid = $('#AddBookId').val()
    let title = $('#AddTitle').val()
    let saleprice = $('#AddSalePrice').val()
    let promotiondate = $('#AddPromotionDate').val()
    let image = $('#AddImage').val()
    console.log('Addsave')
        // let parent = document.getElementById("itemlist")
        // let data = document.createElement("div")

    // data.innerHTML =
    let html = `<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" id="col-${Bookid}">
                    <div class="thumbnail">
                        <h3 id="title-${Bookid}">${title}</h3>
                        <img src="${image}" id="image-${Bookid}">
                        <div class="caption ">
                            <p>
                                <h4>Price : <span id="price-${Bookid}">${saleprice}</span>$ 
                                    <button type="button" class="btn btn-edit" onclick="editBook('${Bookid}')" data-toggle="modal" data-target="#myModalEdit" role="button ">
                                        Edit
                                        <span class="glyphicon glyphicon-pencil "></span>
                                    </button>
                                    <button type="button" class="btn btn-remove" onclick="deleted('${Bookid}')" role="button ">
                                        <span class="glyphicon glyphicon-trash "></span>
                                    </button>
                                </h4>
                            </p>
                        </div>
                    </div>`

    // parent.appendChild(data)

    $('#itemlist').append(html)
    $('#myModalAdd').modal('hide')
}

window.editBook = function(id) {
    let bookName = document.getElementById('title-' + id).textContent
    let bookImg = document.getElementById('image-' + id).currentSrc
    let bookPrice = document.getElementById('price-' + id).textContent
    console.log('editbook')
    $('#editbookid').val(id)
    $('#editbooktitle').val(bookName)
    $('#editbooksaleprice').val(bookPrice)
    $('#editImage').val(bookImg)
}

window.editcomplete = function() {
    console.log('editcomplete')
    let id = $('#editbookid').val()
    let bid = $('#BookId').val()
    let bname = $('#editbooktitle').val()
    let bimg = $('#editbookimage').val()
    let bprice = $('#editbooksaleprice').val()

    $('#title-' + id).html(bname)
    $('#price-' + id).html(bprice)
    $('#image-' + id).attr('src', bimg)

    $('#myModalEdit').modal('hide')
}

window.deleted = function(id) {
        console.log('delete')
        let PopUpAlert = confirm('Are you sure delete item')
        if (PopUpAlert == true) {
            $('#col-' + id).remove()
                // $('#row' + id).remove()
        } else {}
    }
    // console.log('abc')
    // api Add book
window.ApiAddBook = function() {
    let Bookid = $('#AddBookId').val()
    let title = $('#AddTitle').val()
    let saleprice = $('#AddSalePrice').val()
    let promotiondate = $('#AddPromotionDate').val()
    let image = $('#AddImage').val()
    console.log('ApiAddBook')
    let url = 'http://localhost:4000/add/book'
    let data = {
        book_isbn: Bookid,
        book_name: title,
        book_price: saleprice,
        book_image: image,
        book_promotiondate: promotiondate
    }
    console.log('1234')

    fetch(url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            console.log('Success:', response)
            $('#itemlist').html('')
            ApiListBook()
            $('#myModalAdd').modal('hide')
        })
}

// api
window.ApiListBook = function() {
    console.log('ApiListBook')
    let url = 'http://localhost:4000/list/book'
    fetch(url, {
            method: 'get',
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then(res => res.json())
        .then(function(data) {
            console.log('data:', data)
            data.forEach(element => {
                let html = `<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" id="col-${element.book_isbn}">
                    <div class="thumbnail">
                        <h3 id="title-${element.book_isbn}">${element.book_name}</h3>
                        <img src="${element.book_image}" id="image-${element.book_isbn}">
                        <div class="caption ">
                            <p>
                                <h4>Price : <span id="price-${element.book_isbn}">${element.book_price}</span>$ 
                                    <button type="button" class="btn btn-edit" onclick="editBook('${element.book_isbn}')" data-toggle="modal" data-target="#myModalEdit" role="button ">
                                        Edit
                                        <span class="glyphicon glyphicon-pencil "></span>
                                    </button>
                                    <button type="button" class="btn btn-remove" onclick="ApiDelBook('${element.book_isbn}')" role="button ">
                                        <span class="glyphicon glyphicon-trash "></span>
                                    </button>
                                </h4>
                            </p>
                        </div>
                    </div>`
                $('#itemlist').append(html)
            })
        })
        .catch(error => console.error('Error:', error))
        // .then(response => {
        //     console.log('Success:', response)
        // })
}

ApiListBook()

window.ApiEditBook = function() {
    let Bookid = $('#editBookId').val()
    let bid = $('#BookId').val()
    let bname = $('#editebooktitle').val()
    let bimg = $('#editebookimage').val()
    let bprice = $('#editPrice').val()
    let bdate = $('#editebookpromotiondate').val()

    console.log('ApiEditBook')
    let url = 'http://localhost:4000/update/book'
    let data = {
        book_isbn: ebookId,
        book_name: ebookTitle,
        book_price: ebookPrice,
        book_image: ebookImage,
        book_promotiondate: ebookpromotionDate
    }
    console.log(data)

    $('#title-' + BookId).html($('#editebooktitle').val())
    $('#price-' + BookId).html($('#editebooksaleprice').val())
    $('#image-' + BookId).attr('src', $('#editebookimage').val())

    fetch(url, {
            method: 'put',
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            console.log('Success:', response)
            $('#itemlist').html('')
            ApiListBook()
            $('#myModalAdd').modal('hide')
        })

    $('#myModalEdit').modal('hide')
}

// API delete

window.ApiDelBook = function(id) {
    console.log('ApiDelBook')
    let PopUpAlert = confirm('Are you sure delete item')
    if (PopUpAlert == true) {
        let url = 'http://localhost:4000/delete/book'
        let data = { id: id }

        fetch(url, {
                method: 'delete', // or 'PUT'
                body: JSON.stringify(data),
                headers: new Headers({
                    'Content-Type': 'application/json'
                })
            }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => {
                console.log('Success:', response)
                $('#itemlist').html('')
                ApiListBook()
                $('#myModalAdd').modal('hide')
            })
        $('#col-' + id).remove()
            // $('#row-' + id).remove()

    } else {}
}

//   let ebookTitle = $('#editTitle').val()
//   let ebookPrice = $('#editPrice').val()
//   let ebookImage = $('#editImage').val()
//   let ebookPromotionDate = $('editebookpromotiondate').val()
//   let data = {
//     book_isbn: ebookId,
//     book_name: ebookTitle,
//     book_saleprice: ebookPrice,
//     book_image: ebookImage,
//     book_pomotiondate: ebookpromotionDate
//   }
//   console.log(data)

//   fetch(url, {
//     method: 'post',
//     body: JSON.stringify(data),
//     headers: new Headers({
//       'Content-Type': 'application/json'
//     })
//   }).then(res => res.json())
//     .catch(error => console.error('Error:', error))
//     .then(response => {
//       console.log('Success:', response)
//       $('#itemlist').html('')
//       ApiListBook()
//       $('#myModalAdd').modal('hide')
//     })

//   $('#myModalEdit').modal('hide')
// }

// API delete

// window.ApiDeletedBook = function(val) {
//         let PopUpAlert = confirm('Are you sure delete item')
//         if (PopUpAlert == true {
//                 let data = { val: BookId }

//                 fetch(url, {
//                         method: 'post', // or 'PUT'
//                         body: JSON.stringify(data),
//                         headers: new Headers({
//                             'Content-Type': 'application/json'
//                         })
//                     }).then(res => res.json())
//                     .catch(error => console.error('Error:', error))
//                     .then(response => {
//                         console.log('Success:', response)
//                         $('#itemlist').html('')
//                         ApiListBook()
//                         $('#myModalAdd').modal('hide')
//                     })

//                 $('#col-' + val).remove()
//             } else {

//             }
//         }