'use strict'
const Koa = require('koa')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const mysql2 = require('mysql2/promise')
const cors = require('@koa/cors')

const config = require('./src/config/db.json')
const Database = require('./src/lib/db.js')




let main = async() => {
    const app = new Koa()

    let db = await Database(config)


    let router = new Router()
    router.post('/add/book', async(ctx) => {
            //console.log(ctx.request.body)
            let { book_isbn = 0, book_name = '', book_price = 0, book_image = 0, book_promotiondate = 0 } = ctx.request.body

            console.log('ADD', ctx.request.body)

            console.log('Database', db)
            await db.execute(`INSERT INTO books (book_isbn, book_name, book_price, book_image, book_promotiondate) VALUES (?,?,?,?,?)`, [book_isbn, book_name, book_price, book_image, book_promotiondate])

            ctx.body = ctx.request.body
            console.log('OK')

            return
        })
        .get('/list/book', async(ctx) => {
            let [result] = await db.execute(`select * from books`)
            ctx.body = result
        })
        .put('/update/book', async(ctx) => {
            let { book_isbn, book_name, book_price, book_image, book_promotiondate } = ctx.request.body

            if ((book_isbn), (book_name), (book_price), (book_image), (book_promotiondate)) {
                await db.execute(`
                update books set 
                    book_name = ?, book_price = ?, book_imagr = ?, book_promotiondate= ?
                where book_isbn = ?
            `, [book_name, book_price, book_image, book_promotiondate, book_isbn])

                ctx.body = { 'status': 'success' }
                return
            }
        })
        .delete('/delete/book', async(ctx) => {
            let { id } = ctx.request.body

            if (id) {
                await db.execute(`DELETE FROM books where book_isbn = ?`, [id])
                ctx.body = { 'status': 'success' }
                return
            }
        })

    app.use(cors())
    app.use(bodyParser())
    app.use(router.routes())

    app.listen('4000', () => {
        console.log('start server on port 4000')
    })
}

main()





// const dbConfig = require('./config/db')

// const makeTodoCtrl = require('./ctrl/todo')
// const todoRepo = require('./repo/todo')

// const pool = mysql2.createPool(dbConfig)

// const todoCtrl = makeTodoCtrl(pool, todoRepo)

// const router = new Router()
//   .get('/list', todoCtrl.list)
//   .post('/add', todoCtrl.create)
// .get('/todo/:id', todoCtrl.get)
// .patch('/update/:id', todoCtrl.update)
// .delete('/delete', todoCtrl.remove)
// .put('/update/:id/complete', todoCtrl.complete)
// .delete('/todo/:id/complete', todoCtrl.incomplete)

// const app = new Koa()

// app.use(cors({
//   origin: function (ctx) {
//     if (ctx.url === '/test') {
//       return false
//     }
//     return '*'
//   },
//   exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
//   maxAge: 5,
//   credentials: true,
//   allowMethods: ['GET', 'POST', 'DELETE'],
//   allowHeaders: ['Content-Type', 'Authorization', 'Accept']
// }))

// app.use(bodyParser())
// app.use(router.routes())
// app.listen(4000)