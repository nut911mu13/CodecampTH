module.exports = function(pool, repo) {
    return {
        async list(ctx) {
            ctx.body = await repo.list(pool)
        },
        async create(ctx) {
            const todo = ctx.request.body
            //     //console.log(todo);
            //     // TODO: validate todo
            const id = await repo.create(pool, todo)
            ctx.body = todo

        },
        async get(ctx) {
            const id = ctx.params.id
                // TODO: validate id
            let databooks = await repo.find(pool, id)
                // find todo from repo
            ctx.body = todo
            console.log(todo);
            // send todo
        },
        async update(ctx) {
            const id = ctx.params.id
            let { todo } = ctx.request.body
            await repo.changeContent(pool, id, ctx.request.body)

            // changeContent todo from repo
            // ctx.redirect('/todo/'+id)
            // send todo


        },
        async remove(ctx) {
            let id = ctx.params.id
            await repo.remove(pool, id)
                // remove todo from repo
            ctx.body = { id }
                // send todo


        }

    }
}