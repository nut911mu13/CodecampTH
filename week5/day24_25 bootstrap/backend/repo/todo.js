module.exports = {
    create,
    list,
    changeContent,
    remove,

}


async function create(db, databooks) {
    await db.execute(`insert into books (title,saleprice,image) values (?,?,?)`,
     [databooks.title, databooks.saleprice, databooks.image])
    return;
}

async function list(db) {
    let row = await db.execute('select id,title,saleprice,image,promotiondate from books');
    return row[0];
}

async function find(db, id) {
    let results = await db.execute('select * from books where id = ?', [id]);
    return results[0];
}

async function changeContent(db, databooks) {
    await db.execute(`update books set title = ?,saleprice = ?, image =? where id = ?`, [databooks.title, databooks.saleprice, databooks.image, id])
    return;
}

async function remove(db, id) {
    await db.execute(`delete from books where id = ? `, [id])
    return;
}