create table books (
  id int auto_increment,
  title varchar(255) not null,
  saleprice float(10) not null,
  image varchar(255) not null,
  promotiondate timestamp not null default now(),
  primary key (id)
);


