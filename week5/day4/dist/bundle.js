/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lib__ = __webpack_require__(1);

let hello = () => console.log('Hello');
hello();
//example 4-1
// domUtil.setText('app', 'Hello DOM!')
// let hello = () => console.log('Hello')
// hello()
//example 4-2
/* let data = [
  {
    id: 1,
    text: 'List A'
  },
  {
    id: 2,
    text: 'List B'
  },
  {
    id: 3,
    text: 'List C'
  }
]
domUtil.addItems ('app', data)
*/

//example 4-3
// domUtil.editItem('2', 'B List')

//example 4-4
// domUtil.removeItem('3', 'menu')

//example 4-7
/* domUtil.setHTML('app', '<button id="btnOpen">Open</button>')
let btn = document.getElementById('btnOpen')
btn.addEventListener('click', bomUtil.openWindow)
*/
// example 4-8
// document.getElementById("app").innerHTML = "Screen Width: " + screen.width

// example 4-9
/* document.getElementById("app").innerHTML = ` 
The full URL of this page is:<br> ${window.location.href}
host:<br>  ${window.location.hostname}
pathname: <br> ${window.location.pathname}
protocol: <br> ${window.location.protocol}`
*/

// example 4-10
/* document.getElementById("app").innerHTML = ` 
<br> The appName of navigator is : ${window.navigator.appName}
<br> The codeName of navigator is : ${window.navigator.appCodeName}
<br> The platform of platform is : ${window.navigator.platform}`
*/

// example 4-11
/* window.alert("I am an alert box!")
let app = document.getElementById("app")
if (window.confirm("Press a button!")) {
  app.innerHTML = "You pressed OK!"
} else {
  app.innerHTML = "You pressed Cancel!"
}*/

/* setTimeout( () => alert('Hello'), 3000 )
let counter = 0
let timeId = setInterval( () => {
  counter++
  if ( counter > 2) {
    alert(counter)
    clearInterval(timeId)
  }
}, 3000) */

// document.cookie = "username=John Smith; expires=Thu, Wed, 24 Jan 2018 01:35:54 GMT; path=/";

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__domUtil__ = __webpack_require__(2);
/* unused harmony reexport domUtil */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__bomUtil__ = __webpack_require__(3);
/* unused harmony reexport bomUtil */



/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class domUtil {
  constructor() {}
  static setText(id, text) {
    let elem = document.getElementById(id);
    elem.innerText = text;
  }

  static setHTML(id, html) {
    let elem = document.getElementById(id);
    elem.innerHTML = html;
  }

  static addItems(id, data) {
    let parent = document.getElementById(id);
    let ul = document.createElement("ul");
    ul.id = 'menu';

    data.forEach(a => {
      console.log('a', a);
      let li = document.createElement("li");
      //example 4-5
      // li.id = a.id
      // li.style.color = 'blue'
      // li.style.cursor = 'pointer'
      //example 4-6
      // li.addEventListener("click", () => alert(a.text))
      li.appendChild(document.createTextNode(a.text));
      ul.appendChild(li);
    });
    parent.appendChild(ul);
  }
  static editItem(id, text) {
    let elem = document.getElementById(id);
    elem.innerText = text;
  }

  static removeItem(removeId, parentId) {
    let elem = document.getElementById(removeId);
    let parent = document.getElementById(parentId);
    parent.removeChild(elem);
  }
}
/* unused harmony export domUtil */


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
class bomUtil {
  static openWindow() {
    this.win = window.open("https://www.w3schools.com", "", "width=200,height=100");
  }
  static closeWindow() {
    if (this.win) {
      this.win.close();
    }
  }

}
/* unused harmony export bomUtil */


/***/ })
/******/ ]);