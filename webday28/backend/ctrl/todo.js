module.exports = function(pool, repo) {
    return {
        async list(ctx) {
            ctx.body = await repo.list(pool)
        },
        async create(ctx) {
            const todo = ctx.request.body
            console.log(todo);
            // TODO: validate todo
            const todos = await repo.create(pool, todo);
            if (!todo) {
                ctx.throw('error');
            }
            ctx.body = todos;

        },
        async get(ctx) {
            const id = ctx.params.id
                // TODO: validate id
            let todo = await repo.find(pool, id)
                // find todo from repo
            ctx.body = { todo }
            console.log(todo);
            // send todo
        },
        async update(ctx) {
            const id = ctx.params.id
            let { todo } = ctx.request.body
            await repo.changeContent(pool, id, ctx.request.body)

            // changeContent todo from repo
            ctx.redirect('/todo/' + id)
                // send todo


        },
        async remove(ctx) {
            let id = ctx.params.id
            await repo.remove(pool, id)
                // remove todo from repo
            ctx.body = { id }
                // send todo


        },
        async complete(ctx) {
            let id = ctx.params.id
            let result = await repo.markComplete(pool, id)
                // markComplete todo from repo
            ctx.body = { id }
                // send todo

        },
        async incomplete(ctx) {
            let id = ctx.params.id
            let result = await repo.markIncomplete(pool, id)
                // markIncomplete todo from repo
            ctx.body = { id }
                // send todo
        },
    }
}