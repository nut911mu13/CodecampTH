import React, { Component } from 'react';

import { Input, Icon, Button, Card, List ,Spin} from 'antd';

//const URl = "http://localhost:4000/todo" ;

export class Todo extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputText : '',
      listItem: [],
      isLoading: false
    }
   
    this.handleChangeText = this.handleChangeText.bind(this);

  }

  componentDidMount () {
    // เราควรจะ fetch เพื่อเอาค่ามาจาก MockAPI 
    this.fetchGet();
  }

  async fetchGet () {
    const result = await fetch('http://localhost:4000/todo')
    let data = await result.json();
    //console.log(data)
    let listItem = [...data]
    console.log(listItem)
    //let listItem = data.map((value, index) => {
    //return value.contents
   // });

    this.setState({ listItem , isLoading : false})
  }

  async fetchPost (text) {
    this.setState({isLoading :false});
    let data = {"todo" : text };
    const result = await fetch('http://localhost:4000/todo', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })

    if (result.ok) {
      let data = await result.json()
      let listItem = [...this.state.listItem];         
      this.setState({ listItem, isLoading : false })
    }
    
  }

  deleteListAtIndex = async (index,id) => {
    const resp = await fetch ('http://localhost:4000/todo/' + id, {
      method: 'delete'
    });
    if (resp) {
      const result = this.state.listItem;
      result.splice(index, 1);
      this.setState({ listItem: result });
    }
  }

  submitList = async () => {
    await this.fetchPost(this.state.inputText);
    this.fetchGet()
    this.setState({
      //listItem: this.state.listItem.concat([this.state.inputText]),
      inputText: ''
    })
    //console.log(this.state.listItem);
  }

  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.submitList();
    }
  }

  handleChangeText = (event) => {
    this.setState({inputText: event.target.value});
  }

  render() {

  
    return (
        <div>
          { 
            this.state.isLoading === false ? <Card style={{ width: 500 , backgroundColor : this.props.myColor }}>
              <h1>To-do-list</h1>

              <div style={{ marginBottom:'10px'}}>
                <Input
                  onChange={this.handleChangeText}
                  value={this.state.inputText}
                  onKeyPress={this.handleKeyPress}
                  addonAfter={<Button type="primary" onClick={this.submitList}>Add</Button>} />
              </div>

              <List
                bordered
                dataSource={this.state.listItem}
                renderItem={(item,index) => (
                  <List.Item actions={[<a onClick={() => this.deleteListAtIndex(index,item.id)}><Icon type="close-circle" style={{ fontSize: 16, color: 'rgb(255, 145, 0)' }} /></a>]}>
                    {item.todo_message}
                  </List.Item>
              )}
              />
          </Card>:<Spin />
        }
          
        </div>
      );
    }
}
