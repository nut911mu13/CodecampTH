let hw = require('./homework5-1.js');
let assert = require('assert');

describe('tdd lab', function () {
    describe('#checkFileExist()', function () {
        it('should have homework5-1_eyes.json existed', async function () {
            let checkFile = await hw.checkFileExist("homework5-1_eyes.json");
            assert.strictEqual(checkFile, true);
        });
        it('should have homework5-1_gender.json existed', async function () {
            let checkFile = await hw.checkFileExist("homework5-1_gender.json");
            assert.strictEqual(checkFile, true)
        });
        it('should have homework5-1_friends.json existed', async function () {
            let checkFile = await hw.checkFileExist("homework5-1_friends.json");
            assert.strictEqual(checkFile, true)
        });
    });
    describe('#objectKey()', function () {
        it('should have same object key stucture as homework5-1_eyes.json', function () {
            assert.deepEqual(hw.getKeyFromFile("homework5-1_eyes.json"), ["brown", "blue", "green"])
        });
        it('should have same object key stucture as homework5-1_gender.json', function () {
            assert.deepEqual(hw.getKeyFromFile("homework5-1_gender.json"), ["female", "male"])
        });
        it('should have same object key stucture as homework5-1_friends.json', function () {
            assert.deepEqual(hw.getKeyFromFile("homework5-1_friends.json"), ["_id", "friendCount"])
        });
    });
    describe('#userFriendCount()', function () {
        it('should have size of array input as 23', function () {
            assert.deepEqual(hw.userFriendCount(), 23)
        });
    }); 
    describe('#sumOfEyes()', function () {
        it('should have sum of eyes as 23', function () {
            assert.deepEqual(hw.sumOfEyes(), 23)
        });
    });
    describe('#sumOfGender()', function () {
        it('should have sum of gender as 23', function () {
            assert.deepEqual(hw.sumOfGender(), 23)
        });
    });
});