const fs = require('fs');

function checkFileExist(fileName) {
    return new Promise(function (resolve, reject) {
        fs.access(fileName, function (err) {
            if (err)
                reject(err);
            else
                resolve(true);
        });
    });
}

function getJsonData() {
    return new Promise(function (resolve, reject) {
        fs.readFile('homework1-4.json', 'utf8', function (err, rawData) {
            if (err)
                reject(err);
            else
                resolve(JSON.parse(rawData));
        });
    });
}

function saveFile(fileName, resultData, callback) {
    fs.writeFile(fileName, JSON.stringify(resultData), 'utf8', function (err) {
        if (err)
            reject(err);
        else
            callback(err, {
                "fileName": fileName,
                "resultData": resultData
            });
    });
}


//ในไฟล์ มี JSON Object ดังนี้ [{male: 10,female: 16}]
function sumOfGender() {
    let rawData = fs.readFileSync('homework5-1_gender.json', 'utf8');
    let genderData = JSON.parse(rawData);
    return genderData[0].male + genderData[0].female;
}

//ในไฟล์ มี JSON Object ดังนี้ [{"brown":11,"blue":6,"green":6}] 
function sumOfEyes() {
    let rawData = fs.readFileSync('homework5-1_eyes.json', 'utf8');
    let genderData = JSON.parse(rawData);
    return genderData[0].brown + genderData[0].blue + genderData[0].green;
}








//ในไฟล์ มี JSON Object ดังนี้ [{"_id":"5a3711070776d02ed87d2100","friendCount":3},
// {"_id":"5a371107493db1e1fd7a153f","friendCount":1},
// {"_id":"...
function userFriendCount() {
    let rawData = fs.readFileSync('homework5-1_friends.json', 'utf8');
    return JSON.parse(rawData).length;
}

//ในไฟล์ มี JSON Object ดังนี้ [{male: 10,female: 16}]
function getKeyFromFile(fileName) {
    let rawData = fs.readFileSync(fileName, 'utf8');
    return Object.keys(JSON.parse(rawData)[0]);
}

let eyeColorList = [];
let genderList = [];
let friendList = [];
let objEye = {};
let objGender = {};

let getSourceFile = getJsonData();
let eyeFileExist = checkFileExist("homework5-1_eyes.json");
let friendFileExist = checkFileExist("homework5-1_friends.json");
let genderFileExist = checkFileExist("homework5-1_gender.json");
//let GenderEyesCountFileExist = 

Promise
    .all([getSourceFile, eyeFileExist, friendFileExist, genderFileExist])
    // ดึงข้อมูลมาจากไฟล์ และเก็บในตัวแปร data
    .then(([data]) => {
        // นับว่ามีข้อมูลนัยน์ตาแต่ละสีคนละกี่สี แบ่งตามเพศ คืนค่าเป็น Object
        // {male: {brown: 5}}

        // ใช้ Reduce เพืื่อวนลูปใน data และเก็บผลลัพท์ไว้ใน result
        // data.reduce((result, {eyeColor, gender}))
        const eyesCount = data.reduce((result, {eyeColor, gender}) => {
            // ถ้ายังไม่มีเพศใน result ให้กำหนดเป็น Object ว่าง
            if (!result[gender]) {
                result[gender] = {}
            }

            // สร้างตัวแปรให้ชี้ไปที่เพศของตัวเอง result[gender]
            const counts = result[gender]

            // ถ้ามีสีตาในตัวแปร ให้บวกหนึ่ง แต่ถ้าไม่มีให้กำหนดให้เป็น 1
            if (counts[eyeColor]) {
                counts[eyeColor]++
            } else {
                counts[eyeColor] = 1
            }

            // Return ผลลัพธ์กลับไป
            return result
        }, {})

        console.log('Eyes Count', eyesCount)

        
        // for (i = 0; i < socialData.length; i++) {
        //     let obj = socialData[i];
        //     for (let key in obj) {

        //         if (key == "eyeColor") {
        //             if (objEye[obj[key]] > 0) {
        //                 objEye[obj[key]]++;
        //             } else {
        //                 objEye[obj[key]] = 1;
        //             }
        //         }



                //นับว่ามีข้อมูลเพศชายกี่ คน เพศหญิงกี่ คน คืนค่าเป็น Object ดังนี้ {male: 10,female: 16}
                //เซพลงไฟล์ homework51_gender.json
                // if (key == "gender") {
                //     if (objGender[obj[key]] > 0) {
                //         objGender[obj[key]]++;
                //     } else {
                //         objGender[obj[key]] = 1;
                //     }
                // }

                //นับว่ามีเพื่อนทั้งหมดกี่ คนและคืนค่าเป็น Object ดังนี้(array ของ objectตามจำนวนคน) เซพลงไฟล์ homework51_friends.json
                //[{“_id”: ”5a3711070776d02ed87d2100”,”friendCount”: 2}, {“_id”: ”5a3711070776d02ed87d2100”,”friendCount”: 2}]
                // if (key == "friends") {
                //     let objFriend = {};
                //     objFriend._id = obj._id;
                //     objFriend.friendCount = obj.friends.length;
                //     objFriend.tagsCount = obj.tags.length;
                //     objFriend.balance = obj.balance;
                //     friendList.push(objFriend);
                // }


                // นับว่ามีข้อมูลเพศชายกี่คน เพศหญิงกี่คน และแยกสีตา คืนค่าเป็น Object ดังนี ้เซพลงไฟล์ homework5-1_eyes.json
                //{“male”:{“brown”;8, “green” : 6 , “blue” : 8}, “female” :{“brown”:6, “green” : 6, “blue” :8 },}
                
                
                    // let objGenderEyes = {};
                    // objGenderEyes.gender = obj.gender;
                    // objGenderEyes.EyesCount = obj.eyeColor.length;
                    // GenderEyesList.push(objGenderEyes)
                
                    
                     
            // }
        // }





        

        // eyeColorList.push(objEye);
        // genderList.push(objGender);

        // You cannot and should not rely on the ordering of elements within a JSON object.
        // From the JSON specification at http://www.json.org/:
        // "An object is an unordered set of name/value pairs"
        // As a consequence, JSON libraries are free to rearrange the order of the elements as they see fit. This is not a bug.

        saveFile("homework5-1_eyes.json", GenderEyesList, (err, data) => {
        //saveFile("homework5-1_eyes.json", eyeColorList, (err, data) => {
            //console.log(data);
        });
        //console.log(objGenderList);
        saveFile("homework5-1_gender.json", genderList, (err, data) => {
            //console.log(data);
        });
        //console.log(friendList);
        saveFile("homework5-1_friends.json", friendList, (err, data) => {
            //console.log(data);
        });
    })
    .catch(err => console.error("error some promise"));

    let x = sumOfGender();

exports.getKeyFromFile = getKeyFromFile;
exports.checkFileExist = checkFileExist;
exports.userFriendCount = userFriendCount;
exports.sumOfGender = sumOfGender;
exports.sumOfEyes = sumOfEyes;
