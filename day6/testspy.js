assert = require('assert');
sinon = require('sinon');
fs = require('fs');
lab6 = require('./lab6.js');

it('should write JSON string into output6_1.txt. check with spy', function () {
    const save = sinon.spy(fs, "writeFile");
    const user = {
        "firstname": "Somchai",
        "lastname": "Sudlor"
    };
    //save('output6_1.txt', 'Hello, World')
    let dummyCallbackFunction = function (err) { };
    lab6.saveUserDatabase(user, "output6_1.txt", dummyCallbackFunction);
    save.restore();
    sinon.assert.calledOnce(save);
    sinon.assert.calledWith(save, 'output6_1.txt');
});